<?xml version="1.0" encoding="utf-8"?><!--
  ~ Open GPS Tracker
  ~ Copyright (C) 2018  René de Groot
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="presenter"
            type="nl.renedegroot.android.gpstracker.v2.wear.WearControlPresenter" />

        <variable
            name="viewModel"
            type="nl.renedegroot.android.gpstracker.v2.wear.WearViewModel" />

    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:clickable="true"
        android:focusable="true"
        android:onClick="@{() -> presenter.onClickSummary()}"
        android:padding="@dimen/inner_frame_layout_padding"
        app:boxedEdges="all"
        tools:context="nl.renedegroot.android.gpstracker.v2.wear.ControlActivity"
        tools:deviceIds="wear">

        <ImageView
            android:id="@+id/wear_control_status"
            android:layout_width="32dp"
            android:layout_height="32dp"
            android:onClick="@{() -> presenter.onClickControl()}"
            android:src="@{viewModel.state}"
            app:tint="@color/status_icon"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toRightOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/wear_control_top_title"
            android:layout_width="0dp"
            android:layout_height="36dp"
            android:layout_marginTop="24dp"
            android:gravity="center"
            android:textAlignment="center"
            android:textAppearance="@style/TextAppearance.Wearable.Large"
            android:textColor="@color/text"
            android:textSize="28sp"
            app:inverse="@{viewModel.inverseSpeed}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_bias="0.5"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:speed="@{viewModel.averageSpeed}"
            tools:text="3m21s" />


        <TextView
            android:id="@+id/wear_control_left_title"
            android:layout_width="wrap_content"
            android:layout_height="0dp"
            android:layout_marginStart="24dp"
            android:layout_marginTop="8dp"
            android:gravity="center"
            android:textAlignment="center"
            android:textAppearance="@style/TextAppearance.Wearable.Large"
            android:textColor="@color/text"
            android:textSize="28sp"
            app:distance="@{viewModel.distance}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="@+id/wear_control_top_title"
            tools:text="277" />

        <TextView
            android:id="@+id/wear_control_right_title"
            android:layout_width="wrap_content"
            android:layout_height="0dp"
            android:layout_marginEnd="24dp"
            android:gravity="center"
            android:textAlignment="center"
            android:textAppearance="@style/TextAppearance.Wearable.Large"
            android:textColor="@color/text"
            android:textSize="28sp"
            app:wearDuration="@{viewModel.duration}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="@+id/wear_control_left_title"
            tools:text="1i23i" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>

