/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

@file:Suppress("DEPRECATION")

package nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.GpsStatus.GPS_EVENT_FIRST_FIX
import android.location.GpsStatus.GPS_EVENT_SATELLITE_STATUS
import android.location.GpsStatus.GPS_EVENT_STARTED
import android.location.GpsStatus.GPS_EVENT_STOPPED
import android.location.GpsStatus.Listener
import androidx.annotation.RequiresPermission
import nl.renedegroot.android.gpstracker.ng.base.common.onMainThread

class GpsStatusControllerImpl(
        context: Context
) : BaseGpsStatusControllerImpl(context) {

    @SuppressLint("MissingPermission")
    private val callback = Listener { event ->
        when (event) {
            GPS_EVENT_STARTED -> started()
            GPS_EVENT_STOPPED -> stopped()
            GPS_EVENT_FIRST_FIX -> firstFix()
            GPS_EVENT_SATELLITE_STATUS -> {
                val status = locationManager.getGpsStatus(null)
                satellites(status.satellites.count(), status.maxSatellites)
            }
        }
    }

    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    override fun startUpdates(listener: GpsStatusController.Listener) {
        super.listener = listener
        onMainThread {
            locationManager.addGpsStatusListener(callback)
        }
    }

    override fun stopUpdates() {
        stopped()
        locationManager.removeGpsStatusListener(callback)
    }
}
