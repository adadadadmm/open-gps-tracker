/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.base.common.controllers.gpsstatus

import android.Manifest
import androidx.annotation.RequiresPermission

interface GpsStatusController {

    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun  startUpdates(listener: Listener)
    fun  stopUpdates()

    interface Listener {
        fun onStartListening()
        fun onChange(usedSatellites: Int, maxSatellites: Int)
        fun onFirstFix()
        fun onStopListening()
    }
}
