/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.base.track

import nl.renedegroot.android.gpstracker.ng.base.R

class TrackTypeDescriptions {

    companion object {

        const val KEY_META_FIELD_TRACK_TYPE = "SUMMARY_TYPE"
        const val VALUE_TYPE_DEFAULT = "TYPE_DEFAULT"
        const val VALUE_TYPE_WALK = "TYPE_WALK"
        const val VALUE_TYPE_RUN = "TYPE_RUN"
        const val VALUE_TYPE_BIKE = "TYPE_BIKE"
        const val VALUE_TYPE_BOAT = "TYPE_BOAT"
        const val VALUE_TYPE_TRAIN = "TYPE_TRAIN"
        const val VALUE_TYPE_CAR = "TYPE_CAR"

        val defaultType = TrackType(R.drawable.ic_track_type_default, R.string.track_type_default, VALUE_TYPE_DEFAULT)

        val allTrackTypes by lazy {
            listOf(
                    defaultType,
                    TrackType(R.drawable.ic_track_type_walk, R.string.track_type_walk, VALUE_TYPE_WALK),
                    TrackType(R.drawable.ic_track_type_run, R.string.track_type_run, VALUE_TYPE_RUN),
                    TrackType(R.drawable.ic_track_type_bike, R.string.track_type_bike, VALUE_TYPE_BIKE),
                    TrackType(R.drawable.ic_track_type_car, R.string.track_type_car, VALUE_TYPE_CAR),
                    TrackType(R.drawable.ic_track_type_train, R.string.track_type_train, VALUE_TYPE_TRAIN),
                    TrackType(R.drawable.ic_track_type_boat, R.string.track_type_boat, VALUE_TYPE_BOAT)
            )
        }

        fun trackTypeForContentType(contentType: String?): TrackType {
            val trackType = allTrackTypes.find { it.contentValue == contentType }

            return trackType ?: defaultType
        }
    }

    data class TrackType(val drawableId: Int, val stringId: Int, val contentValue: String) {
        fun isRunning() = contentValue == VALUE_TYPE_RUN
    }
}
