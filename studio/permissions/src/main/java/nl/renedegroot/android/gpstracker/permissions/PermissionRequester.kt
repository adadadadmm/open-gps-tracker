/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.permissions

import android.Manifest.permission.ACCESS_BACKGROUND_LOCATION
import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.ACTIVITY_RECOGNITION
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import timber.log.Timber
import javax.inject.Inject

private const val REQUEST_CODE_PERMISSION_REQUESTER = 10001

/**
 * Asks for (Open GPS tracker) permissions
 */
class PermissionRequester @Inject constructor(private val permissionChecker: PermissionChecker) {

    private val explanationLocation: Int = R.string.permissions__explain_needs_location
    private val explanationBackground: Int = R.string.permissions__explain_needs_background_location

    private val grants = MutableLiveData<List<String>>()
    private val isShowingDialog: Boolean
        get() = arrayOf(permissionDialog, missingDialog)
                .fold(false) { acc, dialog -> acc || dialog != null }
    private var permissionDialog: AlertDialog? = null
    private var missingDialog: AlertDialog? = null
    private var request: Array<String> = emptyArray()

    fun startRequest(
            fragment: Fragment,
            permissions: List<String> = listOf(ACCESS_FINE_LOCATION),
            onHadPermissions: (Boolean) -> Unit
    ) =
            if (permissions.all { permissionChecker.checkSelfPermission(fragment.requireContext(), it) }) {
                Timber.d("For start in $fragment validated all permission $permissions")
                onHadPermissions(true)
            } else {
                Timber.d("For $fragment missing some of $permissions")
                grants.observe(fragment.viewLifecycleOwner, object : Observer<List<String>> {
                    override fun onChanged(ignored: List<String>?) {
                        if (permissions.all { permissionChecker.checkSelfPermission(fragment.requireContext(), it) }) {
                            grants.removeObserver(this)
                            Timber.d("For $fragment noticed incoming $permissions")
                            onHadPermissions.invoke(true)
                        } else {
                            onHadPermissions.invoke(false)
                        }
                    }
                })
                checkAccess(fragment, permissions)
            }

    private fun stop(
            fragment: Fragment,
            permissions: List<String> = listOf(ACCESS_FINE_LOCATION),
            onHadPermissions: () -> Unit
    ) {
        permissionDialog?.dismiss()
        permissionDialog = null
        grants.removeObservers(fragment)
        if (permissions.all { permissionChecker.checkSelfPermission(fragment.requireContext(), it) }) {
            Timber.d("For stop in $fragment validated all permission $permissions")
            onHadPermissions.invoke()
        }
    }

    private fun didReceivePermissions(permissions: List<String>) {
        grants.postValue(permissions)
    }

    private fun checkAccess(fragment: Fragment, permissions: List<String>) {
        val activity = fragment.requireActivity()

        val received = permissions.filter { permissionChecker.checkSelfPermission(activity, it) }
        val canAsk = permissions.filter { canAsk(activity, it) }
        val shouldExplain = permissions.filter { permissionChecker.shouldExplain(activity, it) }

        didReceivePermissions(received)

        if (canAsk.isNotEmpty()) {
            showRequest(fragment, canAsk.toTypedArray())
        } else if (shouldExplain.isNotEmpty()) {
            val request = DialogInterface.OnClickListener { _, _ -> showRequest(fragment, shouldExplain.toTypedArray()) }
            val cancel = DialogInterface.OnClickListener { _, _ -> cancel() }
            val message = if (shouldExplain.contains(ACCESS_BACKGROUND_LOCATION)) {
                explanationBackground
            } else {
                explanationLocation
            }
            showRationale(activity, request, cancel, message)
        }
    }

    fun onRequestPermissionsResult(fragment: Fragment, requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        val activity = fragment.activity ?: throw IllegalStateException("Require attached fragment")
        if (requestCode == REQUEST_CODE_PERMISSION_REQUESTER) {
            synchronized(request) {
                request = emptyArray()
                val grants = grantResults.indices
                        .filter { grantResults[it] == PackageManager.PERMISSION_GRANTED }
                        .map { permissions[it] }
                if (permissions.all { grants.contains(it) }) {
                    didReceivePermissions(permissions.filter { permissionChecker.checkSelfPermission(activity, it) })
                } else {
                    val missing = grantResults.indices
                            .filter { grantResults[it] != PackageManager.PERMISSION_GRANTED }
                            .map { permissions[it] }
                    val ok = DialogInterface.OnClickListener { _, _ -> checkAccess(fragment, missing) }
                    showMissing(activity, missing, ok)
                }
            }
        }
    }

    private fun cancel() {
        permissionDialog = null
    }

    private fun showRationale(context: Context, okListener: DialogInterface.OnClickListener, cancel: DialogInterface.OnClickListener, message: Int) {
        if (!isShowingDialog) {
            permissionDialog = AlertDialog.Builder(context)
                    .setMessage(message)
                    .setNegativeButton(R.string.permissions__ignore, cancel)
                    .setPositiveButton(R.string.permissions__grant, okListener)
                    .setOnDismissListener { permissionDialog = null }
                    .show()
        }
    }

    private fun showRequest(fragment: Fragment, permissions: Array<String>) {
        synchronized(request) {
            permissionDialog?.dismiss()
            permissionDialog = null
            if (request.isEmpty() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                request = permissions
                fragment.requestPermissions(request, REQUEST_CODE_PERMISSION_REQUESTER)
            }
        }
    }

    private fun showMissing(context: Context, missing: List<String>, ok: DialogInterface.OnClickListener) {
        if (!isShowingDialog) {
            val description = missing.joinToString(prefix = "\n", separator = "\n", postfix = "\n") { "\u00B7 " + permissionToName(context, it) }
            missingDialog = AlertDialog.Builder(context)
                    .setMessage(context.getString(R.string.permissions__missing, description))
                    .setNegativeButton(R.string.permissions__ignore, null)
                    .setPositiveButton(R.string.permissions__check_again, ok)
                    .setOnDismissListener { missingDialog = null }
                    .show()
        }
    }

    private fun canAsk(activity: Activity, permission: String): Boolean {
        return !permissionChecker.checkSelfPermission(activity, permission) && !permissionChecker.shouldExplain(activity, permission)
    }

    private fun permissionToName(context: Context, permission: String): String =
            when (permission) {
                ACCESS_BACKGROUND_LOCATION -> context.getString(R.string.permissions__permission_background)
                ACCESS_FINE_LOCATION -> context.getString(R.string.permissions__permission_location)
                ACCESS_COARSE_LOCATION -> context.getString(R.string.permissions__permission_location)
                ACTIVITY_RECOGNITION_GMS -> context.getString(R.string.permissions__permission_activity)
                ACTIVITY_RECOGNITION -> context.getString(R.string.permissions__permission_activity)
                else -> permission.removePrefix("android.permission.")
            }
}