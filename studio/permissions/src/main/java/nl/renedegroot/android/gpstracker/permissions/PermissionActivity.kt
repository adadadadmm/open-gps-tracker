/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.permissions

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import nl.renedegroot.android.gpstracker.ng.base.permissions.ARGUMENT_INTENT
import nl.renedegroot.android.gpstracker.ng.base.permissions.ARGUMENT_PERMISSIONS
import nl.renedegroot.android.gpstracker.permissions.databinding.PermissionsFragmentExplainedBinding

class PermissionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(android.R.id.content, permissionFragment(
                            intent.getStringArrayListExtra(ARGUMENT_PERMISSIONS) as ArrayList<String>,
                            intent.getParcelableExtra(ARGUMENT_INTENT) as PendingIntent?
                    ))
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}

class PermissionFragment : Fragment() {

    private val permissionRequester = PermissionRequester(PermissionChecker())
    private val permissions: List<String>
        get() = checkNotNull(requireArguments().getStringArrayList(ARGUMENT_PERMISSIONS))
    private val pendingIntent: PendingIntent?
        get() = requireArguments().getParcelable(ARGUMENT_INTENT)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            DataBindingUtil.inflate<PermissionsFragmentExplainedBinding>(inflater, R.layout.permissions__fragment_explained, container, false)
                    .apply {
                        permissionsButton.setOnClickListener { onClick() }
                        (requireActivity() as PermissionActivity).apply {
                            setSupportActionBar(permissionsToolbar)
                            supportActionBar?.setDisplayHomeAsUpEnabled(true)
                        }
                    }
                    .root

    private fun onClick() {
        permissionRequester.startRequest(this, permissions) { granted ->
            if (granted) {
                pendingIntent?.send(requireContext(), 0, null)
                requireActivity().finish()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionRequester.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
    }
}

private fun permissionFragment(
        permissions: ArrayList<String>,
        pendingIntent: PendingIntent?
) =
        PermissionFragment().also {
            it.arguments = bundleOf(
                    ARGUMENT_PERMISSIONS to permissions,
                    ARGUMENT_INTENT to pendingIntent
            )
        }
