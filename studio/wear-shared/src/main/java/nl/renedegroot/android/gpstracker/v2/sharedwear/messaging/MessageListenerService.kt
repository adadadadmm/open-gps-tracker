/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.v2.sharedwear.messaging

import com.google.android.gms.wearable.DataEventBuffer
import com.google.android.gms.wearable.DataMap
import com.google.android.gms.wearable.MessageEvent
import com.google.android.gms.wearable.WearableListenerService
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatisticsMessage
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatisticsMessage.Companion.PATH_STATISTICS
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatusMessage
import nl.renedegroot.android.gpstracker.v2.sharedwear.model.StatusMessage.Companion.PATH_STATUS
import timber.log.Timber

abstract class MessageListenerService : WearableListenerService() {

    override fun onMessageReceived(messageEvent: MessageEvent?) {
        Timber.d("onMessageReceived($messageEvent: MessageEvent?")
        openMessage(messageEvent?.path, messageEvent?.data)
    }

    override fun onDataChanged(dataEvents: DataEventBuffer?) {
        Timber.d("onDataChanged($dataEvents: DataEventBuffer?")
        dataEvents?.forEach {
            openMessage(it.dataItem.uri.path, it.dataItem.data)
        }
    }

    private fun openMessage(path: String?, data: ByteArray?) {
        if (path != null && data != null) {
            val dataMap = DataMap.fromByteArray(data)
            handleMessageEvents(path, dataMap)
        } else {
            Timber.w("message did not contain data")
        }
    }

    private fun handleMessageEvents(path: String?, dataMap: DataMap) =
            when (path) {
                PATH_STATISTICS ->
                    updateStatistics(StatisticsMessage(dataMap))
                PATH_STATUS ->
                    updateStatus(StatusMessage(dataMap))
                else -> {
                    Timber.e("Failed to recognize path $path")
                }
            }

    abstract fun updateStatus(status: StatusMessage)

    abstract fun updateStatistics(statistics: StatisticsMessage)
}
