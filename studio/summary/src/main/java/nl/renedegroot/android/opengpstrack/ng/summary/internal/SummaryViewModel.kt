package nl.renedegroot.android.opengpstrack.ng.summary.internal

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import nl.renedegroot.android.opengpstrack.ng.summary.R
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary

internal class SummaryViewModel {

    val title = ObservableField("")
    val trackIcon = ObservableInt(R.drawable.ic_track_type_default)
    val duration = ObservableField(0L)
    val distance = ObservableField(0F)
    val averageSpeed = ObservableField(0F)
    val currentSpeed = ObservableField(0F)
    val inverseSpeed = ObservableBoolean(false)
    val trackingControl = ObservableInt(R.string.empty_dash)
    val immersive = ObservableBoolean(true)

    fun showSummary(summary: Summary) {
        title.set(summary.name)
        duration.set(summary.endTimestamp - summary.startTimestamp)
        distance.set(summary.distance)
        averageSpeed.set(summary.distance / (summary.trackedPeriod / 1000L))
        inverseSpeed.set(summary.type.isRunning())
        val recentDeltas = summary.deltas.lastOrNull()?.lastFewOrNull()
        val speed = recentDeltas?.map { it.deltaMeters / (it.deltaMilliseconds / 1000L) }?.average()?.toFloat()
                ?: 0F
        currentSpeed.set(speed)
    }
}

private fun <E> List<E>.lastFewOrNull(few: Int = 2): List<E>? =
         when {
            size >= few -> subList(size - few, size)
            else -> null
        }
