package nl.renedegroot.android.opengpstrack.ng.summary.dagger

import dagger.Component
import nl.renedegroot.android.gpstracker.ng.base.dagger.BaseComponent
import nl.renedegroot.android.opengpstrack.ng.summary.SummaryPresenter
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager

@SummaryScope
@Component(modules = [SummaryModule::class],
        dependencies = [BaseComponent::class])
interface SummaryComponent {

    fun summaryManager(): SummaryManager

    fun inject(summaryPresenter: SummaryPresenter)
}
