/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.service.ng.internal

import android.content.Context

private const val TAG = "opengpstracker:WakeLockTag"

internal class PowerManager(private val context: Context) {
    private var wakeLock: android.os.PowerManager.WakeLock? = null

    fun updateWakeLock(timeout: Long) {
        safeAcquire(timeout)
    }

    fun release() {
        safeRelease()
    }

    @Synchronized
    private fun safeAcquire(timeout: Long) {
        val pm = context.getSystemService(Context.POWER_SERVICE) as android.os.PowerManager
        safeRelease()
        val wakeLock = pm.newWakeLock(android.os.PowerManager.PARTIAL_WAKE_LOCK, TAG)
        wakeLock.acquire(timeout)
        this.wakeLock = wakeLock
    }

    @Synchronized
    private fun safeRelease() {
        wakeLock?.let {
            if (it.isHeld) {
                it.release()
                wakeLock = null
            }
        }

    }

    protected fun finalize() {
        safeRelease()
    }
}
