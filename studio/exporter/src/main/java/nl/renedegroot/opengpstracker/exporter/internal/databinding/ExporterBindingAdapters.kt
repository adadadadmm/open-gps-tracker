package nl.renedegroot.opengpstracker.exporter.internal.databinding

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.Spinner
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import nl.renedegroot.opengpstracker.exporter.R
import nl.renedegroot.opengpstracker.exporter.internal.Status
import nl.renedegroot.opengpstracker.exporter.internal.Target

@BindingAdapter("driveCheckbox")
internal fun CheckBox.setDriveCheckbox(status: Status?) {
    isChecked = listOf(Status.Connected, Status.Running, Status.Finished, Status.Error).contains(status)
    isEnabled = listOf(Status.Prepared, Status.Connected, Status.Finished, Status.Error).contains(status)
}

@BindingAdapter("visible_during_run")
internal fun View.setVisibleDuringRun(status: Status?) {
    visibility = if (status is Status.Running) VISIBLE else GONE
}

@BindingAdapter("visible_after_start")
internal fun View.setVisibleAfterStart(status: Status?) {
    visibility = if (status is Status.Running
            || status is Status.Finished
            || status is Status.Error) {
        VISIBLE
    } else {
        GONE
    }
}

@BindingAdapter("button_status")
internal fun Button.setButtonStatus(status: Status?) =
    when (status) {
        Status.Prepared -> setText(R.string.exporter__export_button_connect_drive)
        Status.Running -> setText(R.string.exporter__export_button_cancel)
        is Status.Connected -> setText(R.string.exporter__export_button_start_export)
        is Status.Finished -> setText(R.string.exporter__export_button_close_export)
        Status.Error -> setText(R.string.exporter__export_button_close_export)
        null -> isEnabled = false
    }

@BindingAdapter("srcCompat")
internal fun ImageView.setTargetImage(target: Target) {
    setImageDrawable(ResourcesCompat.getDrawable(resources, target.drawable, context.theme))
}

@BindingAdapter("adapter")
internal fun Spinner.setTargetAdapter(items: List<Target>) {
    adapter = ArrayAdapter(context, R.layout.exporter__item_spinner, items)
}
