/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.opengpstracker.exporter.internal

import androidx.annotation.DrawableRes
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import nl.renedegroot.opengpstracker.exporter.R
import nl.renedegroot.opengpstracker.exporter.internal.target.ExportProcess

/**
 * View model for the export preparation fragment
 */
internal class ExportViewModel {
    val status = ObservableField<Status>(Status.Prepared)
    val selectedTarget = ObservableField<Target>(Target.GoogleDrive)
    val exportTargets = ObservableField(listOf(Target.GoogleDrive, Target.ContentDrive))

    val completedTracks = ObservableInt(0)
    val totalTracks = ObservableInt(-1)
    val completedWaypoints = ObservableInt(0)
    val totalWaypoints = ObservableInt(-1)


    fun showPrepare() {
        status.set(Status.Prepared)
        completedTracks.set(0)
        totalTracks.set(-1)
        completedWaypoints.set(0)
        totalWaypoints.set(-1)
    }

    fun showActive(state: ExportProcess.ExportState.Active) {
        status.set(Status.Running)
        completedTracks.set(state.completedTracks)
        completedWaypoints.set(state.completedWaypoints)
        totalTracks.set(state.totalTracks)
        totalWaypoints.set(state.totalWaypoints)
    }

    fun showFinished(state: ExportProcess.ExportState.Finished) {
        status.set(Status.Finished)
        completedTracks.set(state.completedTracks)
        completedWaypoints.set(state.completedWaypoints)
    }
}

internal sealed class Status {
    object Prepared : Status()
    object Connected : Status()
    object Running : Status()
    object Finished : Status()
    object Error : Status()
}

internal sealed class Target(@DrawableRes val drawable: Int) {
    object GoogleDrive : Target(R.drawable.exporter__drive_icon) {
        override fun toString() = "Google Drive"
    }

    object ContentDrive : Target(R.drawable.exporter__sdcard_icon) {
        override fun toString() = "SD Card"
    }
}
