/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import nl.renedegroot.android.gpstracker.ng.base.common.onMainThread
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants
import nl.renedegroot.android.gpstracker.service.integration.ContentConstants.Waypoints.WAYPOINTS
import nl.renedegroot.android.gpstracker.service.util.count
import nl.renedegroot.android.gpstracker.service.util.trackUri
import nl.renedegroot.android.gpstracker.service.util.tracksUri
import nl.renedegroot.android.gpstracker.service.util.waypointsUri
import nl.renedegroot.android.gpstracker.utils.contentprovider.append
import nl.renedegroot.opengpstracker.exporter.R
import kotlin.coroutines.cancellation.CancellationException

/**
 * Manager the exporting process
 */
internal class ExportProcess(
        private val contentResolver: ContentResolver
) : Callback {

    private var completedWaypoints = 0
    private var completedTracks = 0
    private var totalWaypoints = 0
    private var totalTracks = 0
    private var exportScope: CoroutineScope? = null

    val state: LiveData<ExportState>
        get() = _state
    private val _state = MutableLiveData<ExportState>()

    init {
        _state.postValue(ExportState.Idle)
    }

    sealed class ExportState {
        object Idle : ExportState()
        class Active(val completedTracks: Int, val totalTracks: Int, val completedWaypoints: Int, val totalWaypoints: Int) : ExportState()
        class Finished(val completedTracks: Int, val completedWaypoints: Int) : ExportState()
        class Error(val message: ErrorMessage) : ExportState() {

        }
    }

    fun startExport(target: ExportTarget) {
        exportScope = CoroutineScope(Dispatchers.IO + SupervisorJob()).apply {
            launch {
                var tracks: Cursor? = null
                try {
                    tracks = contentResolver.query(tracksUri(), arrayOf(ContentConstants.Tracks._ID), null, null, null)
                    if (tracks?.moveToFirst() == true) {
                        completedTracks = 0
                        completedWaypoints = 0
                        totalWaypoints = waypointsUri().count()
                        totalTracks = tracks.count
                        _state.postValue(ExportState.Active(0, totalTracks, 0, totalWaypoints))
                        do {
                            val id = tracks.getLong(0)
                            val trackUri = trackUri(id)
                            launch {
                                target.createUploadTask(contentResolver, trackUri, this@ExportProcess).execute()
                            }

                        } while (tracks.moveToNext() && isActive)
                    } else {
                        _state.postValue(ExportState.Error(ErrorMessage(R.string.exporter__error_tracks_not_found)))
                    }
                } finally {
                    tracks?.close()
                }
            }
        }
    }

    fun stopExport() {
        exportScope?.cancel(CancellationException("Export stopped due to user cancel"))
        exportScope = null
        _state.postValue(ExportState.Error(ErrorMessage(R.string.exporter__error_tracks_was_canceled)))
    }

    override fun onError(message: String) {
        exportScope?.cancel(CancellationException(message))
        exportScope = null
        _state.postValue(ExportState.Error(ErrorMessage(message)))
    }

    override fun onFinished(trackUri: Uri) {
        onMainThread {
            completedTracks += 1
            completedWaypoints += trackUri.append(WAYPOINTS).count()
            if (completedTracks < totalTracks) {
                _state.value = ExportState.Active(completedTracks, totalTracks, completedWaypoints, totalWaypoints)
            } else {
                _state.value = ExportState.Finished(completedTracks, totalWaypoints)
            }
        }
    }
}

internal interface Executable {
    suspend fun execute()

}

internal interface Callback {
    fun onError(message: String)
    fun onFinished(trackUri: Uri)
}
