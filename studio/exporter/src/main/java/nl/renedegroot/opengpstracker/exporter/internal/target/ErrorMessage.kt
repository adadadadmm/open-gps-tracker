/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.opengpstracker.exporter.internal.target

import android.content.res.Resources
import androidx.annotation.StringRes

internal class ErrorMessage private constructor(@StringRes private val messageRes: Int? = null, private val message: String? = null) {

    constructor(messageRes: Int) : this(messageRes = messageRes, message = null)
    constructor(message: String) : this(messageRes = null, message = message)

    fun message(resource: Resources) = message ?: resource.getString(checkNotNull(messageRes))

}