/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.settings

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import nl.renedegroot.android.gpstracker.utils.preference.LoggingInterval
import nl.renedegroot.android.gpstracker.utils.preference.UnitSystem

class SettingsViewModel {
    val customInterval = ObservableBoolean()
    val possibleIntervalPeriod = listOf(LoggingInterval.Second, LoggingInterval.TenSeconds, LoggingInterval.HalfMinute, LoggingInterval.Minute, LoggingInterval.FiveMinutes)
    val selectedIntervalPeriod = ObservableField<LoggingInterval>()

    val localeDescription = ObservableField<StringResource>()
    val possibleUnitSystems = listOf(UnitSystem.Device, UnitSystem.Metric, UnitSystem.Imperial)
    val selectedUnitSystem = ObservableField<UnitSystem>()
    val inverseRunningSpeed = ObservableBoolean()
}