/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.robots

import android.app.Activity
import android.app.Instrumentation
import android.content.Context
import android.content.Intent
import android.content.Intent.CATEGORY_OPENABLE
import android.net.Uri
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasCategories
import androidx.test.espresso.intent.matcher.IntentMatchers.hasType
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.RootMatchers.isPlatformPopup
import androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.MIME_TYPE_ANY
import nl.renedegroot.android.gpstracker.ng.features.tracklist.TrackListViewAdapter
import nl.renedegroot.android.gpstracker.v2.R
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf

fun trackList(block: TrackListRobot.() -> TrackListRobot) = TrackListRobot().apply {
    block()
}

class TrackListRobot() : Robot<TrackListRobot>("TrackList") {

    fun openRowContextMenu(rowNumber: Int): TrackListRobot {
        onView(withId(R.id.fragment_tracklist_list))
                .perform(scrollToPosition<TrackListViewAdapter.ViewHolder>(rowNumber))
                .perform(clickSubView(R.id.row_track_overflow))
        return this
    }

    fun share(): TrackListRobot {
        onView(allOf(withId(R.id.row_track_share), isDisplayed()))
                .perform(click())
        return this
    }

    fun edit(): TrackListRobot {
        onView(allOf(withId(R.id.row_track_edit), isDisplayed()))
                .perform(click())
        return this
    }

    fun delete(): TrackListRobot {
        onView(allOf(withId(R.id.row_track_delete), isDisplayed()))
                .perform(click())
        return this
    }

    fun cancelEdit(): TrackListRobot {
        onView(withId(R.id.fragment_trackEdit_ok))
                .perform(click())

        return this
    }

    fun cancelDelete(): TrackListRobot {
        onView(withId(R.id.fragment_trackdelete_cancel))
                .perform(click())

        return this
    }

    fun isTrackListDisplayed(): TrackListRobot {
        onView(withId(R.id.fragment_tracklist_list))
                .check(matches(isCompletelyDisplayed()))
        return this
    }

    fun openSearch() {
        viewClick(R.id.action_search)
    }

    fun checkWalkingTracking() {
        viewClick(R.id.tracklist__chips_walking)
    }

    fun closeSearch() {
        pressBack()
        pressBack()
    }

    private fun clickSubView(subViewId: Int): ViewAction {
        return object : ViewAction {
            val click = ViewActions.click()

            override fun getDescription(): String {
                return click.description
            }

            override fun getConstraints(): Matcher<View> {
                return click.constraints
            }

            override fun perform(uiController: UiController?, view: View?) {
                click.perform(uiController, view?.findViewById(subViewId))
            }
        }
    }

    fun openExport(context: Context) = apply {
        openActionBarOverflowOrOptionsMenu(context)
        onView(withText(R.string.menu_item_export))
                .perform(click())
    }

    fun openImportDirectory(context: Context) = apply {
        openActionBarOverflowOrOptionsMenu(context)
        onView(withText(R.string.menu_item_import))
                .perform(click())
    }

    fun openImport() = apply {
        viewClick(R.id.fragment_tracklist_import)
    }

    fun openTrackTypeSpinner() = apply {
        viewClick(R.id.fragment_importtracktype_spinner)
    }

    fun selectWalking() = apply {
        onView(withText(R.string.track_type_walk))
                .inRoot(isPlatformPopup())
                .perform(click())
    }

    fun clickDialogOk() = apply {
        viewClick(R.id.fragment_importtracktype_ok)
    }

    fun prepareTrackSelection() = apply {
        intending(allOf(
                hasAction(Intent.ACTION_GET_CONTENT),
                hasCategories(setOf(CATEGORY_OPENABLE)),
                hasType(MIME_TYPE_ANY)
        ))
                .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, Intent("ACTION", Uri.EMPTY)))
    }

    fun prepareDirectorySelection() = apply {
        intending(hasAction(Intent.ACTION_OPEN_DOCUMENT_TREE))
                .respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, Intent("ACTION", Uri.EMPTY)))
    }
}
