/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.util

import androidx.test.espresso.IdlingResource
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import nl.renedegroot.android.gpstracker.ng.base.common.onMainThread
import timber.log.Timber

class IdlingMapResource(map: MapView) : IdlingResource, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveListener, GoogleMap.OnMapLoadedCallback {

    private var isMapLoaded = true
    private var isCameraIdle = true
    private var googleMap: GoogleMap? = null
    private var callback: IdlingResource.ResourceCallback? = null

    init {
        onMainThread {
            map.getMapAsync {
                googleMap = it
                it.setOnCameraIdleListener(this)
                it.setOnCameraMoveListener(this)
                it.setOnMapLoadedCallback(this)
            }
            Timber.d("IdlingMapResource started")
        }
    }

    override fun getName(): String = "MapResource"

    override fun isIdleNow(): Boolean {
        Timber.d("Is idle camera $isCameraIdle && loaded $isMapLoaded")
        return googleMap != null && isCameraIdle && isMapLoaded
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        this.callback = callback
    }

    override fun onCameraIdle() {
        Timber.d("onCameraIdle()")
        isCameraIdle = true
        if (isIdleNow) {
            Timber.d("Became idle")
            callback?.onTransitionToIdle()
        }
    }

    override fun onCameraMove() {
        isCameraIdle = false
    }

    override fun onMapLoaded() {
        Timber.d("onMapLoaded()")
        isMapLoaded = true
        if (isIdleNow) {
            Timber.d("Became idle")
            callback?.onTransitionToIdle()
        }
    }
}
