/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.util

import android.os.Handler
import android.os.Looper
import android.webkit.WebChromeClient
import android.webkit.WebView
import androidx.test.espresso.IdlingResource

class WebViewIdlingResource(private val webView: WebView) : WebChromeClient(), IdlingResource {

    private var callback: IdlingResource.ResourceCallback? = null

    init {
        Handler(Looper.getMainLooper()).post {
            webView.webChromeClient = this@WebViewIdlingResource
        }
    }

    override fun isIdleNow(): Boolean {
        val isIdle = webView.progress == 100
        if (isIdle) {
            callback?.onTransitionToIdle()
        }
        return isIdle
    }

    override fun getName() = "WebViewResource_${webView.id}"

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        this.callback = callback
    }

    override fun onProgressChanged(view: WebView?, newProgress: Int) {
        super.onProgressChanged(view, newProgress)
        if (webView.progress == 100) {
            callback?.onTransitionToIdle()
        }
    }
}
