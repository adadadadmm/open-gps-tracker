/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.util

import android.content.Context
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class ClearPreferences(private val contextProvider: () -> Context) : TestRule {

    private val preferences = listOf<String>("EXPORT_URI", "logging_settings", "settings", "unit_settings")

    override fun apply(base: Statement, description: Description?) = object : Statement() {
        override fun evaluate() {
            val context = contextProvider.invoke()
            preferences.forEach {
                context.deleteSharedPreferences(it)
            }
            base.evaluate()
        }
    }
}
