/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.robots

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.RootMatchers.isPlatformPopup
import androidx.test.espresso.matcher.ViewMatchers.withText
import nl.renedegroot.android.gpstracker.v2.R

fun export(block: ExportRobot.() -> Unit) = ExportRobot().apply { block() }

class ExportRobot : Robot<TrackListRobot>("Export") {

    fun selectSDCard() {
        viewClick(R.id.exporter__sink_selection)
        onView(withText("SD Card"))
                .inRoot(isPlatformPopup())
                .perform(click())
    }

    fun selectGoogleDrive() {
        viewClick(R.id.exporter__sink_selection)
        onView(withText("Google Drive"))
                .inRoot(isPlatformPopup())
                .perform(click())
    }

    fun grantAccess() {
        viewClick(R.id.exporter__fragment_export_drive)
    }

    fun export() {
        viewClick(R.id.exporter__fragment_export_nextStep)
    }
}