/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.robots

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import nl.renedegroot.android.gpstracker.v2.R

fun graph(block: GraphRobot.() -> GraphRobot) {
    GraphRobot().block()
}

class GraphRobot : Robot<GraphRobot>("graph") {
    fun selectTime() {
        onView(withId(R.id.time_selection))
                .perform(click())
    }

    fun selectDistance() {
        onView(withId(R.id.distance_selection))
                .perform(click())
    }
}
