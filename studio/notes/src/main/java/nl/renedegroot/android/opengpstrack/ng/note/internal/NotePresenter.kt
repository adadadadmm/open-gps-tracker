/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.opengpstrack.ng.note.internal

import android.net.Uri
import androidx.core.net.toFile
import kotlinx.coroutines.async
import nl.renedegroot.android.gpstracker.utils.viewmodel.CoroutineViewModel
import java.io.File

internal class NotePresenter(
        private val waypoint: Uri,
        private val noteManager: NoteManager,
        val model: NoteModel = NoteModel(),
        val navigation: NoteNavigation = NoteNavigation()
) : CoroutineViewModel() {

    private var imageNoteUpdate: Uri? = null

    init {
        async {
            model.title.set(noteManager.loadTitleNote(waypoint))
            model.image.set(noteManager.loadImageNote(waypoint))
            model.message.set(noteManager.loadTextNote(waypoint))
        }
    }

    fun onImageClicked() {
        navigation.pickImage()
    }

    fun onSave() {
        updateTitleNote()
        updateImageNote()
        updateMessageNote()

        navigation.dismiss()
    }

    fun onCancel() {
        imageNoteUpdate?.toFile()?.delete()
        navigation.dismiss()
    }

    fun onOk() {
        navigation.dismiss()
    }

    fun onImagesPicked(imageFiles: List<File>) {
        imageFiles.firstOrNull()?.let {
            imageNoteUpdate?.toFile()?.delete()
            imageNoteUpdate = noteManager.createImageNote(it)
            model.image.set(imageNoteUpdate)
        }
    }

    private fun updateTitleNote() {
        val title = model.title.get()
        if (title != null && title.isNotBlank()) {
            noteManager.saveTitleNote(title, waypoint)
        }
    }

    private fun updateImageNote() {
        imageNoteUpdate?.let { noteManager.saveImageNote(it, waypoint) }
    }

    private fun updateMessageNote() {
        val message = model.message.get()
        if (message != null && message.isNotBlank()) {
            val textNote = noteManager.createTextNote(message)
            noteManager.saveTextNote(textNote, waypoint)
        }
    }
}

