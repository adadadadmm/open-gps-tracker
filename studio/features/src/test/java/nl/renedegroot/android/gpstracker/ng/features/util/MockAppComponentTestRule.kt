/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.util

import android.content.ContentResolver
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.base.dagger.BaseComponent
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureComponent
import nl.renedegroot.android.gpstracker.service.dagger.ServiceComponent
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class MockBaseComponentTestRule : TestRule {

    lateinit var mockBaseComponent: BaseComponent

    lateinit var mockServiceComponent: ServiceComponent

    lateinit var mockFeatureComponent: FeatureComponent

    override fun apply(base: Statement, description: Description?): Statement {
        return object : Statement() {
            @Throws(Throwable::class)
            override fun evaluate() {
                mockBaseComponent = mock(BaseComponent::class.java)
                BaseConfiguration.baseComponent = mockBaseComponent
                val mockResolver = mock(ContentResolver::class.java)
                `when`(mockBaseComponent.contentResolver()).thenReturn(mockResolver)
                `when`(mockBaseComponent.computationExecutor()).thenReturn(ImmediateExecutor())
                `when`(mockBaseComponent.uiUpdateExecutor()).thenReturn(ImmediateExecutor())

                mockServiceComponent = mock(ServiceComponent::class.java)
                `when`(mockServiceComponent.providerAuthority()).thenReturn("mock-authority")

                mockFeatureComponent = mock(FeatureComponent::class.java)
                FeatureConfiguration.featureComponent = mockFeatureComponent

                base.evaluate()
            }
        }
    }
}
