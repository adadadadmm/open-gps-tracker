/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.control;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import static nl.renedegroot.android.gpstracker.service.integration.ServiceConstants.STATE_UNKNOWN;


public class ControlViewModelTest {

    private ControlViewModel sut;

    @Before
    public void setup() {
        sut = new ControlViewModel();
    }

    @Test
    public void testInit() {
        // Verify
        Assert.assertEquals(STATE_UNKNOWN, sut.getLoggingState().get());
    }

    @Test
    public void testState() {
        // Execute
        sut.getLoggingState().set(88);

        // Verify
        Assert.assertEquals(88, sut.getLoggingState().get());
    }
}
