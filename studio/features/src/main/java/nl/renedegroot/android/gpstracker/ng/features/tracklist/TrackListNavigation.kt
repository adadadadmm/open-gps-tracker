/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.content.Intent
import android.content.Intent.ACTION_OPEN_DOCUMENT_TREE
import android.net.Uri
import android.os.Build
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.MIME_TYPE_ANY
import nl.renedegroot.android.gpstracker.ng.features.gpxexport.ShareIntentFactory
import nl.renedegroot.android.gpstracker.ng.features.gpximport.Selection
import nl.renedegroot.android.gpstracker.ng.features.gpximport.importTrackTypeDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.track.TrackActivity
import nl.renedegroot.android.gpstracker.ng.features.trackdelete.TrackDeleteDialogFragment
import nl.renedegroot.android.gpstracker.ng.features.trackedit.TrackEditDialogFragment
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.gpstracker.utils.VersionHelper
import nl.renedegroot.android.gpstracker.utils.activityresult.ActivityResultLambda
import nl.renedegroot.android.gpstracker.utils.dialog.informDialog
import nl.renedegroot.opengpstracker.exporter.ExportActivity
import javax.inject.Inject

private const val TAG_DIALOG = "DIALOG"

class TrackListNavigation @Inject constructor() {

    val navigation = MutableLiveData<Consumable<Navigation>>()

    fun finishTrackSelection() {
        navigation.postValue(Consumable(Navigation.Finish))
    }

    fun showTrackEditDialog(track: Uri) {
        navigation.postValue(Consumable(Navigation.Edit(track)))
    }

    fun showTrackDeleteDialog(track: Uri) {
        navigation.postValue(Consumable(Navigation.Delete(track)))
    }

    fun showSharePicker(track: Uri) {
        navigation.postValue(Consumable(Navigation.SharePicker(track)))
    }

    fun startGpxFileSelection() {
        navigation.postValue(Consumable(Navigation.GpxFileSelection))
    }

    fun startGpxDirectorySelection() {
        navigation.postValue(Consumable(Navigation.GpxDirectorySelection))
    }

    fun startExporter() {
        navigation.postValue(Consumable(Navigation.Exporter))
    }

    fun socialShare(track: Uri) {
        navigation.postValue(Consumable(Navigation.SocialShare(track)))
    }

    fun gpxShare(track: Uri) {
        navigation.postValue(Consumable(Navigation.GpxShare(track)))
    }

    fun showSelectionFailure() {
        navigation.postValue(Consumable(Navigation.SelectionFailure))
    }

    fun showImportTypePicker(gpxFileSelection: Selection) {
        navigation.postValue(Consumable(Navigation.ImportType(gpxFileSelection)))
    }
}

sealed class Navigation {
    object Finish : Navigation()
    data class Edit(val track: Uri) : Navigation()
    data class Delete(val track: Uri) : Navigation()
    data class SharePicker(val intent: Uri) : Navigation()
    object GpxFileSelection : Navigation()
    object Dismiss : Navigation()
    object GpxDirectorySelection : Navigation()
    object Exporter : Navigation()
    data class SocialShare(val track: Uri) : Navigation()
    data class GpxShare(val track: Uri) : Navigation()
    object SelectionFailure : Navigation()
    data class ImportType(val selection: Selection) : Navigation()
}

class TrackListNavigator(
        private val activity: FragmentActivity,
        private val childFragmentManager: FragmentManager,
        private val presenter: TrackListPresenter
) {

    @Inject
    lateinit var versionHelper: VersionHelper

    @Inject
    lateinit var shareIntentFactory: ShareIntentFactory

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun observe(owner: LifecycleOwner, navigation: TrackListNavigation) {
        navigation.navigation.observe(owner, { consumable ->
            consumable.consume { destination ->
                navigate(destination)
            }
        })
    }

    private fun navigate(destination: Navigation) =
            when (destination) {
                Navigation.Finish -> finishTrackSelection()
                is Navigation.Edit -> showTrackEditDialog(destination.track)
                is Navigation.Delete -> showTrackDeleteDialog(destination.track)
                is Navigation.SharePicker -> showSharePicker(destination.intent)
                Navigation.GpxFileSelection -> startGpxFileSelection()
                Navigation.GpxDirectorySelection -> startGpxDirectorySelection()
                Navigation.Exporter -> startExporter()
                Navigation.Dismiss -> dismissDialog()
                is Navigation.SocialShare -> socialShare(destination.track)
                is Navigation.GpxShare -> gpxShare(destination.track)
                Navigation.SelectionFailure -> informDialog("Import failed ", "No selection was received.")
                        .show(activity.supportFragmentManager, "ErrorDialog")
                is Navigation.ImportType -> showImportType(destination.selection)
            }

    private fun socialShare(track: Uri) {
        dismissDialog()
        shareIntentFactory.createSocialShareIntent(activity, track)
    }

    private fun gpxShare(track: Uri) {
        dismissDialog()
        shareIntentFactory.createGpxShareIntent(activity, track)
    }

    private fun dismissDialog() {
        val dialog = childFragmentManager.findFragmentByTag(TAG_DIALOG)
        dialog?.let {
            childFragmentManager.commit {
                remove(dialog)
            }
        }
    }

    private fun finishTrackSelection() =
            if (activity is TrackActivity) {
                // For tablet we'll opt to leave the track list on the screen instead of removing it
                // getSupportFragmentManager().popBackStack(TRANSACTION_TRACKS, POP_BACK_STACK_INCLUSIVE);
            } else {
                activity.finish()
            }

    private fun showTrackEditDialog(trackUri: Uri) {
        TrackEditDialogFragment.newInstance(trackUri)
                .show(childFragmentManager, TAG_DIALOG)
    }

    private fun showTrackDeleteDialog(track: Uri) {
        TrackDeleteDialogFragment.newInstance(track)
                .show(childFragmentManager, TAG_DIALOG)
    }

    private fun startGpxFileSelection() {
        if (versionHelper.isAtLeast(Build.VERSION_CODES.KITKAT)) {
            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.addCategory(Intent.CATEGORY_OPENABLE) // Intent.ACTION_OPEN_DOCUMENT has too few results
            intent.type = MIME_TYPE_ANY // Specific GPX mime type will produce zero results
            (activity as ActivityResultLambda).startActivityForResult(intent) { resultIntent ->
                presenter.didSelectGpxFile(resultIntent)
            }
        } else {
            informDialog("Not implemented ", "This feature does not exist pre-KitKat")
                    .show(activity.supportFragmentManager, "ErrorDialog")
        }
    }

    private fun startGpxDirectorySelection() {
        if (versionHelper.isAtLeast(Build.VERSION_CODES.LOLLIPOP)) {
            val intent = Intent(ACTION_OPEN_DOCUMENT_TREE)
            (activity as ActivityResultLambda).startActivityForResult(intent) { resultIntent ->
                presenter.didSelectGpxDirectory(resultIntent)
            }
        } else {
            informDialog("Not implemented ", "This feature does not exist pre-Lollipop")
                    .show(activity.supportFragmentManager, "ErrorDialog")
        }
    }

    private fun showImportType(selection: Selection) {
        importTrackTypeDialogFragment(selection).show(childFragmentManager, TAG_DIALOG)
    }

    private fun startExporter() {
        val intent = Intent(activity, ExportActivity::class.java)
        activity.startActivity(intent)
    }

    private fun showSharePicker(trackUri: Uri) {
        shareChoiceFragment(trackUri)
                .show(childFragmentManager, TAG_DIALOG)
    }
}
