/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map.rendering

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.CornerPathEffect
import android.graphics.Paint
import android.graphics.Path
import androidx.annotation.ColorInt
import androidx.annotation.VisibleForTesting
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary

private const val NORMAL_PATH_SIZE = 250

internal class PathRenderer(
        tileSize: Float,
        private val strokeWidth: Float,
        private val summary: Summary,
        private val toWeight: ((Summary, Int, Pair<Int, Int>) -> Float)?) {
    @get:VisibleForTesting
    val projection: TileProjection = TileProjection(tileSize)
    private val worldPoints: Array<Array<Point>>
    private val isLongTrack: Boolean

    init {
        val worldPoints: Array<Array<Point?>?> = arrayOfNulls(summary.waypoints.size)
        var pathLength = 0
        for (i in summary.waypoints.indices) {
            val segmentWayPoint = summary.waypoints[i]
            pathLength += segmentWayPoint.size
            val segmentWorldPoints = arrayOfNulls<Point>(segmentWayPoint.size)
            worldPoints[i] = segmentWorldPoints
            for (j in segmentWayPoint.indices) {
                val emptyPoint = Point()
                segmentWorldPoints[j] = emptyPoint
                projection.latLngToWorldCoordinates(segmentWayPoint[j], emptyPoint)
            }
        }
        this.worldPoints = worldPoints as Array<Array<Point>>
        isLongTrack = pathLength > NORMAL_PATH_SIZE
    }

    private val solidPaint: Paint by lazy {
        val paint = Paint()
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = this.strokeWidth
        paint.isAntiAlias = true
        paint.pathEffect = CornerPathEffect(10f)
        paint
    }

    fun drawPath(canvas: Canvas, x: Int, y: Int, zoom: Int) {
        // Loop through all points
        var first: Point? = null
        var previous = Point()
        var current = Point()
        for (segment in worldPoints.indices) {
            val worldPointSegment = worldPoints[segment]
            projection.worldToTileCoordinates(worldPointSegment[0], previous, x, y, zoom)
            if (first == null) {
                first = Point(previous)
            }
            if (worldPointSegment.size == 1) {
                continue
            }
            var weightRange = 0 to 0
            var path = Path()
            if (toWeight == null) {
                path.moveTo(previous.x.toFloat(), previous.y.toFloat())
            }
            for (index in 1 until worldPointSegment.size) {
                if (toWeight != null) {
                    path = Path()
                    path.moveTo(previous.x.toFloat(), previous.y.toFloat())
                }
                projection.worldToTileCoordinates(worldPointSegment[index], current, x, y, zoom)
                if (isLongTrack && index < worldPointSegment.size - 1) {
                    if (completeOffscreen(previous, current, canvas)) {
                        // skips parts with both element offscreen
                        path.moveTo(current.x.toFloat(), current.y.toFloat())
                        val tmp = previous
                        previous = current
                        current = tmp
                        continue
                    }
                    if (toCloseTogether(previous, current)) {
                        // or when points are very close together
                        if (toWeight != null) {
                            weightRange = weightRange.first to index
                        }
                        continue
                    }
                }
                path.lineTo(current.x.toFloat(), current.y.toFloat())
                val tmp = previous
                previous = current
                current = tmp
                if (toWeight != null) {
                    weightRange = weightRange.first to index
                    val weight = toWeight.invoke(summary, segment, weightRange)
                    val paint = paintForWeight(weight)
                    canvas.drawPath(path, paint)
                    weightRange = index + 1 to index + 1
                }
            }
            if (toWeight == null) {
                canvas.drawPath(path, solidPaint)
            }
        }
    }

    private fun paintForWeight(weight: Float): Paint {
        val paint = Paint()
        paint.color = weight.asWeightedColor()
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = this.strokeWidth
        paint.isAntiAlias = true
        paint.pathEffect = CornerPathEffect(10f)
        return paint
    }

    private fun toCloseTogether(first: Point, second: Point): Boolean {
        return first.squaredDistanceTo(second) < 25.0
    }

    private fun completeOffscreen(first: Point, second: Point, canvas: Canvas): Boolean {
        var offScreen = false
        if (first.y < 0 && second.y < 0) {
            offScreen = true
        } else if (first.x < 0 && second.x < 0) {
            offScreen = true
        } else if (first.y > canvas.height && second.y > canvas.height) {
            offScreen = true
        } else if (first.x > canvas.width && second.x > canvas.width) {
            offScreen = true
        }

        return offScreen
    }
}

@ColorInt
fun Float.asWeightedColor(): Int =
        Color.HSVToColor(floatArrayOf(coerceIn(0f, 1f) * 122f, 0.9f, 0.9f))
