/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.annotation.TargetApi
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_LOW
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.track.TrackActivity
import nl.renedegroot.android.gpstracker.ng.features.track.newTrackActivityIntent
import nl.renedegroot.android.gpstracker.utils.VersionHelper
import nl.renedegroot.android.opengpstrack.ng.features.R
import timber.log.Timber
import javax.inject.Inject

class ImportNotification(val context: Context) {

    @Inject
    lateinit var versionHelper: VersionHelper
    private var importsToDo = 0

    private val notificationManager: NotificationManager by lazy {
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    private val builder: NotificationCompat.Builder by lazy {
        NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setContentTitle(context.getString(R.string.notification_import_title))
                .setContentText(context.getString(R.string.notification_import_context_ongoing))
                .setSmallIcon(R.drawable.ic_file_download_black_24dp)
                .setProgress(1, 0, true)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setOngoing(true)
                .setTrackListTargetIntent(context)
    }

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    fun didStartImport() {
        Timber.d("fun didStartImport()")
        if (versionHelper.isAtLeast(Build.VERSION_CODES.O)) {
            createChannel()
        }

        importsToDo++
        notificationManager.notify(NOTIFICATION_IMPORT_ID, builder.build())
    }

    fun onProgress(progress: Int, goal: Int) {
        Timber.d("fun onProgress($progress: Int, $goal: Int)")
        builder.setPriority(NotificationCompat.PRIORITY_MIN)
                .setProgress(goal, progress, false)
        notificationManager.notify(NOTIFICATION_IMPORT_ID, builder.build())
    }

    fun didCompleteImport() {
        Timber.d("fun didCompleteImport()")
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentText(context.getString(R.string.notification_import_context_complete))
                .setProgress(0, 0, false)
        importsToDo--
        notificationManager.notify(NOTIFICATION_IMPORT_ID, builder.build())

    }

    fun didFailImport(retry: Boolean) {
        Timber.d("fun didFailImport($retry: Boolean)")
        val contentText = if (retry) {
            context.getString(R.string.notification_import_context_failed_retry)
        } else {
            context.getString(R.string.notification_import_context_failed_error)
        }
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentText(contentText)
                .setProgress(0, 0, false)
        importsToDo--
        notificationManager.notify(NOTIFICATION_IMPORT_ID, builder.build())
    }

    fun dismissCompletedImport() {
        Timber.d("fun dismissCompletedImport()")
        if (importsToDo == 0) {
            notificationManager.cancel(NOTIFICATION_IMPORT_ID)
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createChannel() {
        if (notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID) == null) {
            val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    context.getString(R.string.notification_operation_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT)
            channel.lockscreenVisibility = VISIBILITY_PUBLIC
            channel.name = context.getString(R.string.notification_operation_channel_name)
            channel.description = context.getString(R.string.notification_operation_channel_description)
            channel.enableLights(false)
            channel.enableVibration(false)
            channel.importance = IMPORTANCE_LOW
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun NotificationCompat.Builder.setTrackListTargetIntent(context: Context): NotificationCompat.Builder {
        val resultPendingIntent = if (context.resources.getBoolean(R.bool.track_map_multi_pane)) {
            val intent = newTrackActivityIntent(context, true)
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        } else {
            val stackBuilder = TaskStackBuilder.create(context)
            stackBuilder.addParentStack(TrackActivity::class.java)
            stackBuilder.addNextIntent(Intent(context, TrackActivity::class.java))
            stackBuilder.addNextIntent(Intent(context, TrackListActivity::class.java))
            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)!!
        }
        setContentIntent(resultPendingIntent)

        return this
    }

    companion object {

        private const val NOTIFICATION_CHANNEL_ID = "import_notification"
        private val NOTIFICATION_IMPORT_ID = R.string.notification_import_title

    }
}
