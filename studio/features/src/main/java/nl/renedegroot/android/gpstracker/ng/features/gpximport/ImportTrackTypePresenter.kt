/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpximport

import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.ViewModel
import kotlinx.android.parcel.Parcelize
import nl.renedegroot.android.gpstracker.ng.base.BaseConfiguration
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions.Companion.KEY_META_FIELD_TRACK_TYPE

class ImportTrackTypePresenter(
        private val selection: Selection
) : ViewModel() {

    val model = ImportTrackTypeModel()
    val onItemSelectedListener: AdapterView.OnItemSelectedListener by lazy {
        object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                model.selectedPosition.set(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                model.selectedPosition.set(AdapterView.INVALID_POSITION)
            }
        }
    }

    fun ok() {
        model.dismiss.value = true
        val i = model.selectedPosition.get()
        val trackType = if (i == AdapterView.INVALID_POSITION) {
            TrackTypeDescriptions.allTrackTypes[0].contentValue
        } else {
            TrackTypeDescriptions.allTrackTypes[i].contentValue
        }
        when (selection) {
            is Selection.Directory -> importDirectory(selection.intent.selectioinUri(trackType), trackType)
            is Selection.File -> importFile(selection.intent.selectioinUri(trackType), trackType)
        }
    }

    fun cancel() {
        model.dismiss.value = true
    }

    private fun Intent.selectioinUri(trackType: String): Uri {
        putExtra(KEY_META_FIELD_TRACK_TYPE, trackType)
        val takeFlags = flags and Intent.FLAG_GRANT_READ_URI_PERMISSION
        val uri = requireNotNull(data) { "Missing data from import selection" }
        val resolver = BaseConfiguration.baseComponent.contentResolver()
        if (action.fromStorageFramework()) {
            resolver.takePersistableUriPermission(uri, takeFlags)
        }

        return uri
    }
}

sealed class Selection : Parcelable {
    @Parcelize
    data class Directory(val intent: Intent) : Selection()

    @Parcelize
    data class File(val intent: Intent) : Selection()
}

private fun String?.fromStorageFramework() =
        this == Intent.ACTION_OPEN_DOCUMENT || this == Intent.ACTION_OPEN_DOCUMENT_TREE