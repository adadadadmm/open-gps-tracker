/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.graphs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.permissions.PermissionRequester
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentGraphsBinding
import javax.inject.Inject

class GraphsFragment : Fragment() {

    @Inject
    lateinit var permissionRequester: nl.renedegroot.android.gpstracker.permissions.PermissionRequester
    private lateinit var presenter: GraphsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        presenter = ViewModelProvider(this).get(GraphsPresenter::class.java)
        FeatureConfiguration.featureComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = DataBindingUtil.inflate<FragmentGraphsBinding>(inflater, R.layout.fragment_graphs, container, false)
        binding.viewModel = presenter.viewModel
        binding.presenter = presenter

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    companion object {

        @JvmStatic
        fun newInstance(): GraphsFragment {
            return GraphsFragment()
        }
    }
}
