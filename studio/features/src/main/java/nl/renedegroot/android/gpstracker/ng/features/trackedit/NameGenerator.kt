/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.trackedit

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import nl.renedegroot.android.gpstracker.ng.base.location.LocationFactory
import nl.renedegroot.android.gpstracker.permissions.PermissionHelper
import nl.renedegroot.android.opengpstrack.ng.features.R
import java.text.SimpleDateFormat
import java.util.Calendar
import javax.inject.Inject
import javax.inject.Named

class NameGenerator @Inject constructor(val context: Context,
                                        @Named("dayFormatter") private val dayFormat: SimpleDateFormat,
                                        private val locationFactory: LocationFactory,
                                        private val permissionHelper: PermissionHelper) {

    fun generateName(now: Calendar): String {
        val today = dayFormat.format(now.time)
        val period = period(now)
        val location = locationName()

        return if (location == null) {
            context.getString(R.string.initial_time_track_name, today, period)
        } else {
            context.getString(R.string.initial_time_location_track_name, today, period, location)
        }
    }

    @SuppressLint("MissingPermission")
    private fun locationName(): String? =
            if (permissionHelper.hasPermission(arrayListOf(Manifest.permission.ACCESS_COARSE_LOCATION))) {
                locationFactory.getLocationName()
            } else {
                null
            }

    private fun period(now: Calendar): String =
            when (now.get(Calendar.HOUR_OF_DAY)) {
                in 0..4 -> context.getString(R.string.period_night)
                in 5..11 -> context.getString(R.string.period_morning)
                in 12..17 -> context.getString(R.string.period_afternoon)
                in 18..23 -> context.getString(R.string.period_evening)
                else -> ""
            }
}
