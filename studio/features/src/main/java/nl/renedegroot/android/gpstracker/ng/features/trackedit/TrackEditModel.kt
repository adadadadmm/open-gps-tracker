/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.trackedit

import android.net.Uri
import android.widget.AdapterView.INVALID_POSITION
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import nl.renedegroot.android.gpstracker.ng.base.track.TrackTypeDescriptions
import nl.renedegroot.android.gpstracker.utils.Consumable

class TrackEditModel {
    var trackUri = ObservableField<Uri?>()
    val name = ObservableField("")
    val selectedPosition = ObservableInt(INVALID_POSITION)
    val trackTypes = TrackTypeDescriptions.allTrackTypes
    val dismissed = MutableLiveData<Consumable<Boolean>>()

    interface View {
        fun dismiss()
    }
}
