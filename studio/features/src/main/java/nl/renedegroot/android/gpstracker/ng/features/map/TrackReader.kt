/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map

import android.net.Uri
import android.os.AsyncTask
import com.google.android.gms.maps.model.LatLngBounds
import nl.renedegroot.android.gpstracker.ng.base.location.LatLng
import nl.renedegroot.android.gpstracker.service.util.DefaultResultHandler
import nl.renedegroot.android.gpstracker.service.util.ResultHandler
import nl.renedegroot.android.gpstracker.service.util.asLatLng
import nl.renedegroot.android.gpstracker.service.util.readTrack

class TrackReader(internal val trackUri: Uri, private val action: (String, LatLngBounds, List<List<LatLng>>) -> Unit)
    : AsyncTask<Void, Void, ResultHandler>() {


    var isFinished = false
        private set

    override fun doInBackground(vararg p: Void): ResultHandler? {
        val handler = DefaultResultHandler()
        if (isCancelled) return null
        trackUri.readTrack(handler)
        if (isCancelled) return null
        val points = handler.waypoints.map { list -> list.map { it.asLatLng() } }
        val name = handler.name ?: ""
        action(name, handler.bounds, points)
        if (isCancelled) return null

        return handler
    }

    override fun onPostExecute(result: ResultHandler?) {
        super.onPostExecute(result)
        isFinished = true
    }

    override fun onCancelled() {
        super.onCancelled()
        isFinished = true
    }
}


