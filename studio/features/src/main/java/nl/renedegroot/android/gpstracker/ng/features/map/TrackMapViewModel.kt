/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.map

import android.net.Uri
import android.view.View
import androidx.annotation.StringRes
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.google.android.gms.maps.model.LatLngBounds
import nl.renedegroot.android.gpstracker.ng.base.location.LatLng
import nl.renedegroot.android.gpstracker.ng.features.map.editwaypoint.WaypointSelection
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.note.NoteMarker

class TrackMapViewModel {
    val trackUri: ObservableField<Uri?> = ObservableField()
    val name: ObservableField<String> = ObservableField("")
    val summary = ObservableField<Summary>()
    val completeBounds = ObservableField<LatLngBounds?>()
    val trackHead = ObservableField<LatLng?>()
    val showSatellite = ObservableBoolean(false)
    val willLock = ObservableBoolean(false)
    val isLocked = ObservableBoolean(false)
    val weightType = ObservableField<WeightType>(WeightType.Speed)
    val notes = ObservableField<List<NoteMarker>>()
    val selectedWaypoint = ObservableField<WaypointSelection?>()
    val controlsVisibility = ObservableInt(View.VISIBLE)
    val waypointSelectionVisibility = ObservableInt(View.GONE)

    fun selectedWaypoint(waypoint: WaypointSelection) {
        selectedWaypoint.set(waypoint)
        controlsVisibility.set(View.GONE)
        waypointSelectionVisibility.set(View.VISIBLE)
    }

    fun deselectWaypoint(){
        selectedWaypoint.set(null)
        controlsVisibility.set(View.VISIBLE)
        waypointSelectionVisibility.set(View.GONE)

    }
}

sealed class WeightType {
    abstract fun toggle(): WeightType
    @get:StringRes
    abstract val minimal: Int
    @get:StringRes
    abstract val maximal: Int

    object Speed : WeightType() {
        override val minimal = R.string.weight_type_speed_min
        override val maximal = R.string.weight_type_speed_max
        override fun toggle() = Altitude
    }

    object Altitude : WeightType() {
        override val minimal = R.string.weight_type_height_min
        override val maximal = R.string.weight_type_height_max
        override fun toggle() = None
    }

    object None : WeightType() {
        override val minimal = R.string.weight_type_none
        override val maximal = R.string.blank
        override fun toggle() = Speed
    }
}
