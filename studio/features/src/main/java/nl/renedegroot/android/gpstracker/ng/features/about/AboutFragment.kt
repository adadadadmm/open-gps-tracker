/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.about

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentAboutBinding

const val ABOUT_TAG = "AboutFragmentFragmentTag"

/**
 * Show a little HTML with licenses and version info
 */
class AboutFragment : DialogFragment() {
    
    private val aboutModel = AboutModel()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
            DataBindingUtil.inflate<FragmentAboutBinding>(layoutInflater, R.layout.fragment_about, null, false).let { binding ->
                binding.model = aboutModel
                return AlertDialog.Builder(requireContext())
                        .setView(binding.root)
                        .setPositiveButton(android.R.string.ok) { _, _ -> dismiss() }
                        .create()
            }
}
