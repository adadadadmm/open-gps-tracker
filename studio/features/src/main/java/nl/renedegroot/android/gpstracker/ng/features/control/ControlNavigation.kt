package nl.renedegroot.android.gpstracker.ng.features.control

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.opengpstrack.ng.features.R
import javax.inject.Inject

class ControlNavigation @Inject constructor() {

    internal val step = MutableLiveData<Consumable<ControlNavigationStep>>()

    fun showStopLoggingConfirmation() {
        step.postValue(Consumable(ControlNavigationStep.ConfirmStop))
    }
}

class ControlNavigator(
        private val fragment: Fragment
) {
    fun observe(controlNavigation: ControlNavigation) {
        controlNavigation.step.observe(fragment.viewLifecycleOwner, { consumable ->
            consumable.consume { navigate(it) }
        })
    }

    private fun navigate(step: ControlNavigationStep) {
        when (step) {
            ControlNavigationStep.ConfirmStop -> {
                ConfirmStepDialog()
                        .show(fragment.childFragmentManager, "ConfirmStepDialog")
            }
        }
    }
}

internal sealed class ControlNavigationStep {
    object ConfirmStop : ControlNavigationStep()
}

internal class ConfirmStepDialog : DialogFragment() {

    private val presenter
        get() = ViewModelProvider(requireParentFragment()).get(ControlPresenter::class.java)

    override fun onCreateDialog(savedInstanceState: Bundle?) =
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.control_confirm_stop_title)
                    .setNegativeButton(R.string.control_confirm_stop_negative) { _, _ -> }
                    .setPositiveButton(R.string.control_confirm_stop_positive) { _, _ -> presenter.stopLogging() }
                    .create()
}
