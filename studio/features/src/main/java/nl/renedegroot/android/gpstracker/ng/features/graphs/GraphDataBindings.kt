/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.graphs

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.graphs.dataproviders.GraphDataCalculator
import nl.renedegroot.android.gpstracker.ng.features.graphs.widgets.LineGraph
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.opengpstrack.ng.features.R

private fun statisticsFormatter(): StatisticsFormatter = FeatureConfiguration.featureComponent.statisticsFormatter()

@BindingAdapter("date")
fun setDate(textView: TextView, timeStamp: Long?) {
    if (timeStamp == null || timeStamp == 0L) {
        textView.text = textView.context.getText(R.string.empty_dash)
    } else {
        textView.text = statisticsFormatter().convertTimestampToDate(textView.context, timeStamp)
    }
}

@BindingAdapter("time")
fun setTime(textView: TextView, timeStamp: Long?) {
    if (timeStamp == null || timeStamp == 0L) {
        textView.text = textView.context.getText(R.string.empty_dash)
    } else {
        textView.text = statisticsFormatter().convertTimestampToTime(textView.context, timeStamp)
    }
}

@BindingAdapter("timeSpan")
fun setTimeSpan(textView: TextView, timeStamp: Long?) {
    if (timeStamp == null || timeStamp == 0L) {
        textView.text = textView.context.getText(R.string.empty_dash)
    } else {
        textView.text = statisticsFormatter().convertTimestampToTime(textView.context, timeStamp)
    }
}

@BindingAdapter("graphDuration")
fun setDuration(textView: TextView, timeStamp: Long?) {
    if (timeStamp == null || timeStamp <= 0L) {
        textView.text = textView.context.getText(R.string.empty_dash)
    } else {
        textView.text = statisticsFormatter().convertSpanDescriptiveDuration(textView.context, timeStamp)
    }
}

@BindingAdapter("graphDistance")
fun setDistance(textView: TextView, distance: Float?) {
    if (distance == null || distance <= 0L) {
        textView.text = textView.context.getText(R.string.empty_dash)
    } else {
        textView.text = statisticsFormatter().convertMetersToDistance(textView.context, distance)
    }
}

@BindingAdapter("speed", "inverse")
fun setSpeed(textView: TextView, speed: Float?, inverse: Boolean?) {
    if (speed == null || speed <= 0L) {
        textView.text = textView.context.getText(R.string.empty_dash)
    } else {
        textView.text = statisticsFormatter().convertMeterPerSecondsToSpeed(
                textView.context,
                speed,
                inverse ?: false)

    }
}

@BindingAdapter("activated")
fun setActivated(view: View, activated: Boolean?) {
    view.isActivated = activated ?: false
}

@BindingAdapter("provider")
fun setProvider(graph: LineGraph, graphDataProvider: GraphDataCalculator?) {
    val provider = graphDataProvider ?: GraphDataCalculator.DefaultGraphValueDescriptor
    graph.xUnit = graph.context.getString(provider.xLabel)
    graph.yUnit = graph.context.getString(provider.yLabel)
    graph.description = provider
    graph.data = provider.graphPoint
}
