/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.track

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.ViewModelProvider
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.ng.features.model.TrackSearch
import nl.renedegroot.android.gpstracker.permissions.PermissionHelper
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.ActivityTrackMapBinding
import javax.inject.Inject

private const val KEY_SELECTED_TRACK_URI = "KEY_SELECTED_TRACK_URI"
private const val KEY_SELECTED_TRACK_NAME = "KEY_SELECTED_TRACK_NAME"
private const val ARG_SHOW_TRACKS = "ARG_SHOW_TRACKS"

fun newTrackActivityIntent(context: Context, showTracks: Boolean): Intent {
    val intent = Intent(context, TrackActivity::class.java)
    intent.putExtra(ARG_SHOW_TRACKS, showTracks)

    return intent
}

class TrackActivity : AppCompatActivity() {

    private lateinit var presenter: TrackPresenter
    private var startWithOpenTracks: Boolean = false
    private val optionMenuObserver: Observable.OnPropertyChangedCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            invalidateOptionsMenu()
        }
    }

    @Inject
    lateinit var trackSearch: TrackSearch

    @Inject
    lateinit var permissionHelper: PermissionHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        FeatureConfiguration.featureComponent.inject(this)
        super.onCreate(savedInstanceState)

        presenter = ViewModelProvider(this).get(TrackPresenter::class.java)
        permissionHelper.supportPermissionScreen(this)
        val binding = DataBindingUtil.setContentView<ActivityTrackMapBinding>(this, R.layout.activity_track_map)
        setSupportActionBar(binding.toolbar)
        binding.toolbar.bringToFront()

        TrackNavigator(this).observe(presenter.navigation)
        presenter.viewModel.name.addOnPropertyChangedCallback(optionMenuObserver)
        binding.viewModel = presenter.viewModel

        if (savedInstanceState == null) {
            startWithOpenTracks = intent.getBooleanExtra(ARG_SHOW_TRACKS, false)
        } else {
            startWithOpenTracks = false
            val uri = savedInstanceState.getParcelable<Uri>(KEY_SELECTED_TRACK_URI)
            val name = savedInstanceState.getString(KEY_SELECTED_TRACK_NAME)
            presenter.viewModel.trackUri.set(uri)
            presenter.viewModel.name.set(name)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
        if (startWithOpenTracks) {
            presenter.navigation.showTrackSelection()
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun onDestroy() {
        presenter.viewModel.name.removeOnPropertyChangedCallback(optionMenuObserver)

        super.onDestroy()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent?.action == Intent.ACTION_SEARCH) {
            trackSearch.query.value = intent.getStringExtra(SearchManager.QUERY)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_SELECTED_TRACK_URI, presenter.viewModel.trackUri.get())
        outState.putString(KEY_SELECTED_TRACK_NAME, presenter.viewModel.name.get())
    }

    //region Context menu

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.track, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                presenter.onEditOptionSelected()
                true
            }
            R.id.action_about -> {
                presenter.onAboutOptionSelected()
                true
            }
            R.id.action_list -> {
                presenter.onListOptionSelected()
                true
            }
            R.id.action_graphs -> {
                presenter.onGraphsOptionSelected()
                true
            }
            R.id.action_settings -> {
                presenter.onSettingsOptionSelected()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    //endregion
}
