/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.trackdelete

import android.content.ContentResolver
import android.net.Uri
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractTrackPresenter
import nl.renedegroot.android.gpstracker.service.util.readName
import javax.inject.Inject

class TrackDeletePresenter : AbstractTrackPresenter() {

    internal val viewModel = TrackDeleteModel()
    @Inject
    lateinit var contentResolver: ContentResolver
    @Inject
    lateinit var summaryManager: SummaryManager

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    @WorkerThread
    override suspend fun onChange() {
        trackUri?.let {
            loadTrackName(it)
        }
    }

    fun ok() {
        trackUri?.let {
            deleteTrack(it)
            summaryManager.invalidateCache(it)
            viewModel.dismiss.set(true)
        }
    }

    fun cancel() {
        viewModel.dismiss.set(true)
    }

    private fun loadTrackName(trackUri: Uri) {
        val trackName = trackUri.readName()
        viewModel.trackUri.set(trackUri)
        viewModel.name.set(trackName)
    }

    private fun deleteTrack(trackUri: Uri) {
        contentResolver.delete(trackUri, null, null)
    }
}
