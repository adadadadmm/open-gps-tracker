/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentSharechoiceBinding

private const val ARG_TRACK_URI = "ARG_TRACK_URI"

fun shareChoiceFragment(trackUri: Uri) = ShareChoiceFragment().apply {
    arguments = bundleOf(ARG_TRACK_URI to trackUri)
}

class ShareChoiceFragment : BottomSheetDialogFragment() {

    private val trackUri: Uri
        get() = checkNotNull(requireArguments().getParcelable(ARG_TRACK_URI))

    private lateinit var trackListPresenter: TrackListPresenter

    private var binding: FragmentSharechoiceBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            DataBindingUtil.inflate<FragmentSharechoiceBinding>(inflater, R.layout.fragment_sharechoice, container, false).apply {
                binding = this
                track = trackUri
                presenter = trackListPresenter
            }.root

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    fun configure(presenter: TrackListPresenter) {
        trackListPresenter = presenter
    }
}