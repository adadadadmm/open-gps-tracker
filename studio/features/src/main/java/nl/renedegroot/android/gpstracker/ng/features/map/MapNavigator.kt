package nl.renedegroot.android.gpstracker.ng.features.map

import android.net.Uri
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.note.newNoteFragmentInstance

private const val TAG_WAYPOINT_NOTE = "TAG_WAYPOINT_NOTE"

sealed class MapNavigation {
    object ShowTrackingControls : MapNavigation()
    object HideTrackingControls : MapNavigation()
    data class CreateWaypointNote(val waypoint: Uri) : MapNavigation()
    data class ShowWaypointNote(val waypoint: Uri) : MapNavigation()
}

class MapNavigator(private val fragmentManager: FragmentManager) : Observer<MapNavigation> {
    override fun onChanged(navigation: MapNavigation) =
            when (navigation) {
                is MapNavigation.CreateWaypointNote -> createWaypointNote(navigation.waypoint)
                is MapNavigation.ShowWaypointNote -> showWaypointNote(navigation.waypoint)
                MapNavigation.ShowTrackingControls -> showTrackingControls()
                MapNavigation.HideTrackingControls -> hideTrackingControls()
            }

    private fun hideTrackingControls() {
        val fragment = checkNotNull(fragmentManager.findFragmentById(R.id.fragment_control)) { "Missing control fragment" }
        fragmentManager.beginTransaction()
                .hide(fragment)
                .commit()

    }

    private fun showTrackingControls() {
        val fragment = checkNotNull(fragmentManager.findFragmentById(R.id.fragment_control)) { "Missing control fragment" }
        fragmentManager.beginTransaction()
                .show(fragment)
                .commit()
    }

    private fun createWaypointNote(waypoint: Uri) {
        val dialog = newNoteFragmentInstance(waypoint, editable = true)
        dialog.show(fragmentManager, TAG_WAYPOINT_NOTE)
    }

    private fun showWaypointNote(waypoint: Uri) {
        val dialog = newNoteFragmentInstance(waypoint, editable = false)
        dialog.show(fragmentManager, TAG_WAYPOINT_NOTE)
    }
}
