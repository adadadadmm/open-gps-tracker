/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.tracklist

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import android.net.Uri
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import nl.renedegroot.android.gpstracker.ng.features.map.rendering.TrackPolylineProvider
import nl.renedegroot.android.gpstracker.service.util.asGoogleLatLng
import nl.renedegroot.android.gpstracker.utils.formatting.StatisticsFormatter
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.summary.summary.Summary

class TrackViewState(val uri: Uri) {
    val name = ObservableField<String>("")

    val iconType = ObservableInt(R.drawable.ic_track_type_default)
    val startDay = ObservableField<String>("--")
    val duration = ObservableField<String>("--")
    val distance = ObservableField<String>("--")
    val completeBounds = ObservableField<LatLngBounds?>()
    val waypoints = ObservableField<List<List<LatLng>>>(listOf())
    val polylines = ObservableField<List<PolylineOptions>?>()
    val editMode = ObservableBoolean(false)

    fun setSummary(context: Context, statisticsFormatter: StatisticsFormatter, summary: Summary) {
        completeBounds.set(summary.bounds)
        val listOfLatLng = summary.waypoints.map { segment -> segment.map { waypoint -> waypoint.asGoogleLatLng() } }
        waypoints.set(listOfLatLng)
        val trackPolylineProvider = TrackPolylineProvider(listOfLatLng)
        polylines.set(trackPolylineProvider.lineOptions)
        iconType.set(summary.type.drawableId)
        name.set(summary.name)

        startDay.set(statisticsFormatter.convertTimestampToStart(context, summary.startTimestamp))
        var duration = context.getString(R.string.empty_dash)
        if (summary.startTimestamp in 1 until summary.endTimestamp) {
            duration = statisticsFormatter.convertSpanDescriptiveDuration(context, summary.endTimestamp - summary.startTimestamp)
        }
        this.duration.set(duration)
        var distance = context.getString(R.string.empty_dash)
        if (summary.distance > 0) {
            distance = statisticsFormatter.convertMetersToDistance(context, summary.distance)
        }
        this.distance.set(distance)
    }
}
