/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.gpximport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import nl.renedegroot.android.gpstracker.utils.viewmodel.viewModel
import nl.renedegroot.android.opengpstrack.ng.features.R
import nl.renedegroot.android.opengpstrack.ng.features.databinding.FragmentImportTracktypeDialogBinding

private const val SELECTION = "SELECTION"

fun importTrackTypeDialogFragment(selection: Selection) = ImportTrackTypeDialogFragment().apply {
    arguments = bundleOf(SELECTION to selection)
}

class ImportTrackTypeDialogFragment : DialogFragment() {

    private val presenter
        get() = viewModel { ImportTrackTypePresenter(selection) }

    private val selection: Selection
        get() = checkNotNull(requireArguments().getParcelable(SELECTION))

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentImportTracktypeDialogBinding>(inflater, R.layout.fragment_import_tracktype_dialog, container, false)
        binding.presenter = presenter
        binding.model = presenter.model
        presenter.model.dismiss.observe(viewLifecycleOwner) { shouldDismiss ->
            if (shouldDismiss) {
                dismiss()
            }
        }
        binding.fragmentImporttracktypeSpinner.onItemSelectedListener = presenter.onItemSelectedListener

        return binding.root
    }
}
