/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.activityrecognition

import android.app.PendingIntent
import android.content.Context
import android.net.Uri
import com.google.android.gms.location.ActivityRecognitionClient
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionRequest
import com.google.android.gms.location.DetectedActivity
import timber.log.Timber
import javax.inject.Inject

class ActivityRecognition @Inject constructor(
        private val context: Context,
        private val client: ActivityRecognitionClient) {

    private var pendingIntent: PendingIntent? = null

    fun start(trackUri: Uri) {
        stop()
        pendingIntent = createPendingIntent(trackUri)
        val task = client.requestActivityTransitionUpdates(createRequest(), pendingIntent)!!
        task.addOnSuccessListener { Timber.i("Successfully request activity transition updates") }
        task.addOnFailureListener { exception -> Timber.e("Failed to request activity transition updates. Error status $exception") }
    }

    private fun createPendingIntent(trackUri: Uri): PendingIntent? =
            PendingIntent.getService(
                    context,
                    0,
                    recognitionIntent(context, trackUri),
                    PendingIntent.FLAG_CANCEL_CURRENT
            )

    private fun createRequest(): ActivityTransitionRequest {
        val transitions = listOf(
                ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.WALKING)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build(),
                ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.RUNNING)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build(),
                ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.ON_BICYCLE)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build(),
                ActivityTransition.Builder()
                        .setActivityType(DetectedActivity.IN_VEHICLE)
                        .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                        .build()
        )

        return ActivityTransitionRequest(transitions)
    }

    fun stop() {
        client.removeActivityUpdates(pendingIntent)
    }
}
