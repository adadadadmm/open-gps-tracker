/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.ng.features.trackedit

import android.net.Uri
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.INVALID_POSITION
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.WorkerThread
import nl.renedegroot.android.gpstracker.ng.features.dagger.FeatureConfiguration
import nl.renedegroot.android.gpstracker.utils.viewmodel.AbstractTrackPresenter
import nl.renedegroot.android.gpstracker.service.util.readName
import nl.renedegroot.android.gpstracker.service.util.readTrackType
import nl.renedegroot.android.gpstracker.service.util.saveTrackType
import nl.renedegroot.android.gpstracker.service.util.updateName
import nl.renedegroot.android.gpstracker.utils.Consumable
import nl.renedegroot.android.opengpstrack.ng.summary.summary.SummaryManager
import javax.inject.Inject

class TrackEditPresenter : AbstractTrackPresenter() {

    @Inject
    lateinit var summaryManager: SummaryManager
    val viewModel = TrackEditModel()
    val onItemSelectedListener: AdapterView.OnItemSelectedListener by lazy {
        object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.selectedPosition.set(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                viewModel.selectedPosition.set(INVALID_POSITION)
            }
        }
    }

    init {
        FeatureConfiguration.featureComponent.inject(this)
    }

    @WorkerThread
    override suspend fun onChange() {
        viewModel.trackUri.set(trackUri)
        loadTrackTypePosition(trackUri)
        loadTrackName(trackUri)
    }

    fun ok(trackUri: Uri, trackName: String) {
        saveTrackName(trackUri, trackName)
        saveTrackTypePosition(trackUri)
        summaryManager.invalidateCache(trackUri)
        viewModel.dismissed.value = Consumable(true)
    }

    fun cancel() {
        viewModel.dismissed.value = Consumable(true)
    }

    private fun loadTrackName(trackUri: Uri?) {
        if (trackUri != null) {
            val trackName = trackUri.readName()
            viewModel.name.set(trackName)
        } else {
            viewModel.name.set("")
        }
    }

    private fun loadTrackTypePosition(trackUri: Uri?) {
        if (trackUri != null) {
            val trackType = trackUri.readTrackType()
            val position = viewModel.trackTypes.indexOfFirst { it == trackType }
            viewModel.selectedPosition.set(position)
        } else {
            viewModel.selectedPosition.set(INVALID_POSITION)
        }
    }

    private fun saveTrackName(trackUri: Uri, trackName: String) {
        trackUri.updateName(trackName)
    }

    private fun saveTrackTypePosition(trackUri: Uri) {
        val trackType = viewModel.trackTypes[viewModel.selectedPosition.get()]
        trackUri.saveTrackType(trackType)
    }

    data class ViewHolder(val imageView: ImageView, val textView: TextView)
}
