/*
 * Open GPS Tracker
 * Copyright (C) 2020  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package nl.renedegroot.android.gpstracker.ng.features.map.editwaypoint

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import nl.renedegroot.android.gpstracker.service.util.Waypoint
import nl.renedegroot.android.opengpstrack.ng.note.R
import javax.inject.Inject

class WaypointMarkerController @Inject constructor(context: Context) {

    private val pin = checkNotNull(ContextCompat.getDrawable(context, R.drawable.ic_selected)).toBitmap(128, 128)

    fun createSelection(waypoint: Waypoint): WaypointSelection = WaypointSelection(
            waypoint,
            MarkerOptions()
                    .position(LatLng(waypoint.latitude, waypoint.longitude))
                    .anchor(0.5F, 0.5F)
                    .icon(BitmapDescriptorFactory.fromBitmap(pin))
    )

}

data class WaypointSelection(val waypoint: Waypoint, private val markerOptions: MarkerOptions) {
    private var marker: Marker? = null

    fun removeFromMap() {
        marker?.remove()
    }

    fun addToMap(googleMap: GoogleMap): Marker {
        val marker = googleMap.addMarker(markerOptions)
        this.marker = marker
        return marker
    }
}