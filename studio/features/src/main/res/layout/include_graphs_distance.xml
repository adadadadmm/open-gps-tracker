<?xml version="1.0" encoding="utf-8"?><!--
  ~ Open GPS Tracker
  ~ Copyright (C) 2018  René de Groot
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="presenter"
            type="nl.renedegroot.android.gpstracker.ng.features.graphs.GraphsPresenter" />

        <variable
            name="viewModel"
            type="nl.renedegroot.android.gpstracker.ng.features.graphs.GraphsViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="8dp"
        app:layout_scrollFlags="scroll|snap|enterAlways">

        <ImageView
            android:id="@+id/distance_selection"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:background="?android:attr/selectableItemBackground"
            android:clickable="true"
            android:focusable="true"
            android:onClick="@{() -> presenter.didSelectDistance()}"
            app:activated="@{viewModel.distanceSelected}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="@+id/graph_label_distance"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <ImageView
            android:id="@+id/time_selection"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:background="?android:attr/selectableItemBackground"
            android:clickable="true"
            android:focusable="true"
            android:onClick="@{() -> presenter.didSelectTime()}"
            app:activated="@{viewModel.durationSelected}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="@+id/graph_label_time"
            app:layout_constraintStart_toEndOf="@+id/graph_label_distance"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/graph_label_distance"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:text="@string/graph_label_distance"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toLeftOf="@+id/graph_label_time"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_scrollFlags="scroll|snap"
            tools:text="Distance" />

        <TextView
            android:id="@+id/graph_label_points"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:text="@string/graph_label_points"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_label_speed"
            app:layout_constraintLeft_toRightOf="@+id/graph_label_speed"
            app:layout_constraintRight_toRightOf="parent"
            tools:text="Points" />

        <TextView
            android:id="@+id/graph_label_speed"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:text="@string/graph_label_speed"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_label_distance"
            app:layout_constraintLeft_toRightOf="@+id/graph_label_time"
            app:layout_constraintRight_toLeftOf="@+id/graph_label_points"
            tools:text="Speed" />

        <TextView
            android:id="@+id/graph_value_distance"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="7dp"
            android:layout_marginBottom="8dp"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:graphDistance="@{viewModel.distance}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toLeftOf="@+id/graph_value_time"
            app:layout_constraintTop_toBottomOf="@+id/graph_label_distance"
            tools:text="123km" />

        <TextView
            android:id="@+id/graph_value_start_time"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:inverse="@{viewModel.inverseSpeed}"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_value_distance"
            app:layout_constraintLeft_toRightOf="@+id/graph_value_time"
            app:layout_constraintRight_toLeftOf="@+id/graph_value_points"
            app:speed="@{viewModel.speed}"
            tools:text="56 kmh" />

        <TextView
            android:id="@+id/graph_value_points"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:text="@{viewModel.waypoints}"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_value_start_time"
            app:layout_constraintLeft_toRightOf="@+id/graph_value_start_time"
            app:layout_constraintRight_toRightOf="parent"
            tools:text="5432" />

        <TextView
            android:id="@+id/graph_label_time"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:text="@string/graph_label_time"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_label_distance"
            app:layout_constraintLeft_toRightOf="@+id/graph_label_distance"
            app:layout_constraintRight_toLeftOf="@+id/graph_label_speed"
            tools:text="Time" />

        <TextView
            android:id="@+id/graph_value_time"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:graphDuration="@{viewModel.timeSpan}"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_value_distance"
            app:layout_constraintLeft_toRightOf="@+id/graph_value_distance"
            app:layout_constraintRight_toLeftOf="@+id/graph_value_start_time"
            tools:text="1h23m" />

    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>
