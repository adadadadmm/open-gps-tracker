<?xml version="1.0" encoding="utf-8"?><!--
  ~ Open GPS Tracker
  ~ Copyright (C) 2018  René de Groot
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~  but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~  GNU General Public License for more details.
  ~
  ~  You should have received a copy of the GNU General Public License
  ~  along with this program. If not, see <http://www.gnu.org/licenses/>.
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="nl.renedegroot.android.gpstracker.ng.features.graphs.GraphsViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_scrollFlags="scroll|snap|enterAlways">

        <TextView
            android:id="@+id/graph_label_date"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:text="@string/graph_label_day"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintHorizontal_chainStyle="spread"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toLeftOf="@+id/graph_label_time"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/graph_label_max_speed"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:text="@string/graph_label_maxspeed"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_label_time"
            app:layout_constraintLeft_toRightOf="@+id/graph_label_time"
            app:layout_constraintRight_toLeftOf="@+id/graph_label_pause"
            tools:text="Max speed" />

        <TextView
            android:id="@+id/graph_label_pause"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:text="@string/graph_label_paused"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_label_max_speed"
            app:layout_constraintLeft_toRightOf="@+id/graph_label_max_speed"
            app:layout_constraintRight_toRightOf="parent"
            tools:text="Paused" />

        <TextView
            android:id="@+id/graph_label_time"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:text="@string/graph_label_start_time"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Subtitle1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_label_date"
            app:layout_constraintLeft_toRightOf="@+id/graph_label_date"
            app:layout_constraintRight_toLeftOf="@+id/graph_label_max_speed" />

        <TextView
            android:id="@+id/graph_value_start_day"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="8dp"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:date="@{viewModel.startTime}"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toLeftOf="@+id/graph_value_start_time"
            app:layout_constraintTop_toBottomOf="@+id/graph_label_date"
            tools:text="30-1-17" />

        <TextView
            android:id="@+id/graph_value_start_time"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_value_start_day"
            app:layout_constraintLeft_toRightOf="@+id/graph_value_start_day"
            app:layout_constraintRight_toLeftOf="@+id/graph_value_max_speed"
            app:time="@{viewModel.startTime}"
            tools:text="9:54" />

        <TextView
            android:id="@+id/graph_value_max_speed"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:inverse="@{viewModel.inverseSpeed}"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_value_start_time"
            app:layout_constraintLeft_toRightOf="@+id/graph_value_start_time"
            app:layout_constraintRight_toLeftOf="@+id/graph_value_pause"
            app:speed="@{viewModel.maxSpeed}"
            tools:text="56 kph" />

        <TextView
            android:id="@+id/graph_value_pause"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:textAppearance="@style/TextAppearance.MaterialComponents.Body1"
            app:graphDuration="@{viewModel.paused}"
            app:layout_constraintBaseline_toBaselineOf="@+id/graph_value_max_speed"
            app:layout_constraintLeft_toRightOf="@+id/graph_value_max_speed"
            app:layout_constraintRight_toRightOf="parent"
            tools:text="12m" />

    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>
