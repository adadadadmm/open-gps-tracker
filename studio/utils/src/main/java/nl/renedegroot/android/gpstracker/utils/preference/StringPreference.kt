/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.utils.preference

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import kotlin.reflect.KProperty

fun stringPreference(context: Context, preferenceName: String, key: String, defaultValue: String) =
        StringPreferenceDelegate(context, preferenceName, key, defaultValue)

class StringPreferenceDelegate(
        context: Context,
        preferenceName: String,
        key: String,
        defaultValue: String
) {

    private val preference by lazy {
        StringPreference(
                context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE),
                key,
                defaultValue
        )
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): String? =
            preference.read()

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String?) {
        if (value == null) {
            preference.clear()
        } else if (value != preference.read()) {
            preference.write(value)
        }
    }
}

private class StringPreference(
        private val preferences: SharedPreferences,
        private val key: String,
        private val defaultValue: String
) {

    fun read(): String =
            preferences.getString(key, defaultValue) ?: defaultValue

    fun write(value: String) =
            preferences.edit { putString(key, value) }

    fun clear() =
            preferences.edit { remove(key) }

}
