/*
 * Open GPS Tracker
 * Copyright (C) 2019 René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses>.
 */

package nl.renedegroot.android.gpstracker.utils.file

import android.content.Context
import android.os.Environment.DIRECTORY_DOCUMENTS
import android.os.Environment.DIRECTORY_PICTURES
import androidx.core.content.ContextCompat
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException

fun Context.filesDir(path: String, fileName: String): File =
        try {
            fileOutputStream(fileName, externalFilesDir(), path)
        } catch (e: IOException) {
            fileOutputStream(fileName, filesDir, path)
        }

fun Context.cacheFile(path: String, fileName: String): File =
        try {
            fileOutputStream(fileName, cacheDir, path)
        } catch (e: IOException) {
            fileOutputStream(fileName, cacheDirCompat(), path)
        }

private fun Context.externalFilesDir() =
        ContextCompat.getExternalFilesDirs(this, DIRECTORY_DOCUMENTS).first()
                ?: throw FileNotFoundException()

private fun Context.cacheDirCompat() =
        ContextCompat.getExternalCacheDirs(this).first() ?: throw FileNotFoundException()

private fun fileOutputStream(fileName: String, directory: File, path: String): File {
    val outputDirectory = File(directory, path)
    outputDirectory.mkdirs()
    val exportFile = File(outputDirectory, fileName)
    exportFile.delete()
    exportFile.isFile || exportFile.createNewFile()
    return exportFile
}
