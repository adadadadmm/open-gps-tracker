/*
 * Open GPS Tracker
 * Copyright (C) 2018  René de Groot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package nl.renedegroot.android.gpstracker.utils.formatting

import android.content.Context
import android.text.SpannableString
import android.text.format.DateFormat
import android.text.style.RelativeSizeSpan
import nl.renedegroot.android.gpstracker.utils.preference.UnitSettingsPreferences
import nl.renedegroot.android.test.utils.R
import java.util.Date
import kotlin.math.roundToInt

private const val msPerMinute = 1000L * 60L
private const val msPerHour = msPerMinute * 60L
private const val msPerDay = msPerHour * 24L
private const val msPerSecond = 1000L

class StatisticsFormatter(
        private val localeProvider: LocaleProvider,
        private val preferences: UnitSettingsPreferences,
        private val timeSpanUtil: TimeSpanCalculator = TimeSpanCalculator()
) {

    private fun formatSmall100Meters() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_small_100_meters
            } else {
                R.string.units__imperial_format_small_100_meters
            }

    private fun formatBig100KilometerCompact() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_big_100_kilometer_compact
            } else {
                R.string.units__imperial_format_big_100_kilometer_compact
            }
    
    private fun formatBigKilometerCompact() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_big_kilometer_compact
            } else {
                R.string.units__imperial_format_big_kilometer_compact
            }

    private fun formatSmall100MetersCompact() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_small_100_meters_compact
            } else {
                R.string.units__imperial_format_small_100_meters_compact
            }

    private fun formatSmallMetersCompact() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_small_meters_compact
            } else {
                R.string.units__imperial_format_small_meters_compact
            }

    fun convertMetersAltitude(context: Context, meters: Float): String {
        val convert = preferences.meterToSmallDistance

        return context.getString(formatSmall100Meters()).format(localeProvider.locale, meters / convert)
    }

    fun convertMetersToCompactDistance(context: Context, meters: Float): String {
        val distance: String
        distance = when {
            meters >= 100000 -> {
                val convert = preferences.meterToBigDistance
                context.getString(formatBig100KilometerCompact()).format(localeProvider.locale, meters / convert)
            }
            meters >= 1000 -> {
                val convert = preferences.meterToBigDistance
                context.getString(formatBigKilometerCompact()).format(localeProvider.locale, meters / convert)
            }
            meters >= 100 -> {
                val convert = preferences.meterToSmallDistance
                context.getString(formatSmall100MetersCompact()).format(localeProvider.locale, meters / convert)
            }
            meters > 0 -> {
                val convert = preferences.meterToSmallDistance
                context.getString(formatSmallMetersCompact()).format(localeProvider.locale, meters / convert)
            }
            else -> {
                context.getString(R.string.empty_dash)
            }
        }
        return distance
    }

    private fun formatBig100Kilometer() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_big_100_kilometer
            } else {
                R.string.units__imperial_format_big_100_kilometer
            }

    private fun formatBigKilometer() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_big_kilometer
            } else {
                R.string.units__imperial_format_big_kilometer
            }

    private fun formatSmallMeters() =
            if (preferences.useMetrics) {
                R.string.units__metrics_format_small_meters
            } else {
                R.string.units__imperial_format_small_meters
            }

    fun convertMetersToDistance(context: Context, meters: Float): String {
        val distance: String
        distance = when {
            meters >= 100000 -> {
                val convert = preferences.meterToBigDistance
                context.getString(formatBig100Kilometer()).format(localeProvider.locale, meters / convert)
            }
            meters >= 1000 -> {
                val convert = preferences.meterToBigDistance
                context.getString(formatBigKilometer()).format(localeProvider.locale, meters / convert)
            }
            meters >= 100 -> {
                val convert = preferences.meterToSmallDistance
                context.getString(formatSmall100Meters()).format(localeProvider.locale, meters / convert)
            }
            meters > 0 -> {
                val convert = preferences.meterToSmallDistance
                context.getString(formatSmallMeters()).format(localeProvider.locale, meters / convert)
            }
            else -> {
                context.getString(R.string.empty_dash)
            }
        }
        return distance
    }

    fun convertTimestampToStart(context: Context, timestamp: Long?): String =
            if (timestamp == null || timestamp == 0L) {
                context.getString(R.string.empty_dash)
            } else {
                timeSpanUtil.getRelativeTimeSpanString(timestamp).toString()
            }


    fun convertSpanToCompactDuration(context: Context, msDuration: Long): String {
        if (msDuration == 0L) {
            return context.getString(R.string.empty_dash)
        }
        val days = (msDuration / msPerDay).toInt()
        val hours = ((msDuration - (days * msPerDay)) / msPerHour).toInt()
        val minutes = ((msDuration - (days * msPerDay) - (hours * msPerHour)) / msPerMinute).toInt()
        val seconds = ((msDuration - (days * msPerDay) - (hours * msPerHour) - (minutes * msPerMinute)) / msPerSecond).toInt()

        return when {
            days > 0 -> context.getString(R.string.days_compact, days, hours)
            hours > 0 -> context.getString(R.string.hours_compact, hours, minutes)
            else -> context.getString(R.string.minutes_compact, minutes, seconds)
        }
    }


    fun convertSpanDescriptiveDuration(context: Context, msDuration: Long): String {
        if (msDuration == 0L) {
            return context.getString(R.string.empty_dash)
        }
        val days = (msDuration / msPerDay).toInt()
        val hours = ((msDuration - (days * msPerDay)) / msPerHour).toInt()
        val minutes = ((msDuration - (days * msPerDay) - (hours * msPerHour)) / msPerMinute).toInt()
        val seconds = ((msDuration - (days * msPerDay) - (hours * msPerHour) - (minutes * msPerMinute)) / msPerSecond).toInt()
        var duration: String
        if (days > 0) {
            duration = context.resources.getQuantityString(R.plurals.track_duration_days, days, days)
            if (hours > 0) {
                duration += " "
                duration += context.resources.getQuantityString(R.plurals.track_duration_hours, hours, hours)
            }
        } else if (hours > 0) {
            duration = context.resources.getQuantityString(R.plurals.track_duration_hours, hours, hours)
            if (minutes > 0) {
                duration += " "
                duration += context.resources.getQuantityString(R.plurals.track_duration_minutes, minutes, minutes)
            }
        } else if (minutes > 0) {
            duration = context.resources.getQuantityString(R.plurals.track_duration_minutes, minutes, minutes)
        } else {
            duration = context.resources.getQuantityString(R.plurals.track_duration_seconds, seconds, seconds)
        }

        return duration
    }

    fun convertMeterPerSecondsToSpeed(context: Context,
                                      meterPerSecond: Float,
                                      isRunning: Boolean,
                                      decimals: Boolean = true): String {
        return if (meterPerSecond > 0) {
            if (isRunning && preferences.useInverseRunningSpeed) {
                val runnerSpeed = (1F / meterPerSecond) / 0.06F
                val totalSeconds = (runnerSpeed * 60).roundToInt()
                val minutes = totalSeconds / 60
                val seconds = totalSeconds % 60
                val unit = context.resources.getString(R.string.speed_runners_unit)
                context.getString(R.string.format_runners_speed).format(localeProvider.locale, minutes, seconds, unit)
            } else {
                val conversion = preferences.mpsToSpeed
                val speed = meterPerSecond * conversion
                val unit = preferences.speedUnit
                val format = when {
                    !decimals -> "%.0f %s"
                    speed < 9.95 -> "%.2f %s"
                    speed < 99.5 -> "%.1f %s"
                    else -> "%.0f %s"
                }
                format.format(localeProvider.locale, speed, unit)
            }
        } else {
            context.getString(R.string.empty_dash)
        }
    }

    fun convertTimestampToDate(context: Context, startTimestamp: Long): String {
        val date = Date(startTimestamp)
        return DateFormat.getDateFormat(context).format(date)
    }

    fun convertTimestampToTime(context: Context, startTimestamp: Long): String {
        val date = Date(startTimestamp)
        return DateFormat.getTimeFormat(context).format(date)
    }
}

fun String.asSmallLetterSpans(): SpannableString {
    val spannable = SpannableString(this)
    var start: Int? = null
    for (i in indices) {
        if (!this[i].isDigit() && start == null) {
            start = i
        }
        if (this[i].isDigit() && start != null) {
            spannable.setSpan(RelativeSizeSpan(0.5f), start, i, 0)
            start = null
        }
    }
    if (start != null) {
        spannable.setSpan(RelativeSizeSpan(0.5f), start, this.length, 0)
    }

    return spannable
}
